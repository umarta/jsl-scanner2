package com.jsl.scanner.data.remote.response.move

class FetchResultData(

    var header: Header?,
    var detail: ArrayList<Detail?>

) {

    class Header(
        var id: Int,
        var transaksi_id: Int,
        var warehouse_from_id: String,
        var warehouse_to_id: String,
        var kode_warehouse_from: String,
        var kode_warehouse_to: String,
        var doc_no: String,
        var doc_status: String,
        var author: String
    )

    class Detail(
        var id: String,
        var scan_id: String,
        var no: String,
        var kode_produk: String,
        var locator_to_id: String,
        var kode_locator_to: String,
        var locator_from_id: String,
        var kode_locator_from: String,
        var qty_idem: String,
        var qty_scan: String,
        var product_name: String,
        var sku: String
    )

}
