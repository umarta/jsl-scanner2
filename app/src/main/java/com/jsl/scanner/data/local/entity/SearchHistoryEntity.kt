//package com.jsl.scanner.data.local.entity
//
//import androidx.room.ColumnInfo
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import java.util.*
//
//@Entity(tableName = "searchHistory")
//class SearchHistoryEntity(
//    @PrimaryKey
//    @ColumnInfo(name = "id")
//    var id: String = UUID.randomUUID().toString(),
//
//    @ColumnInfo(name = "userID")
//    var userID: String?,
//
//    @ColumnInfo(name = "query")
//    var query: String?,
//
//    @ColumnInfo(name = "count")
//    var count: Long,
//
//    @ColumnInfo(name = "status")
//    var status: String?,
//
//    @ColumnInfo(name = "date")
//    var date: Long?,
//
//    @ColumnInfo(name = "isDelete")
//    var isDelete: Boolean? = false
//)