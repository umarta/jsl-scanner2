package com.jsl.scanner.data.remote.response.scan

class CheckStockData(
    val product_code: String? = null,
    val locator_name: String? = null,
    val warehouse_code: String? = null,
    val qty: Float? = null,
) {
}