package com.jsl.scanner.data.user

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AnalyticsManager @Inject constructor(private val firebaseAnalytics: FirebaseAnalytics) {
    companion object{
        const val HOME_BANNER = "home_banner"
    }

    fun track(logEvent: String, bundle: Bundle) = firebaseAnalytics.logEvent(logEvent, bundle)
}