package com.jsl.scanner.data.move

import com.jsl.scanner.data.local.Storage
import com.jsl.scanner.data.remote.response.move.ChangeLocator
import com.jsl.scanner.data.remote.response.move.FetchResultData
import com.jsl.scanner.utils.Constants
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoveManager @Inject constructor(
    private val storage: Storage
) {
    fun moveInventSaveData(moveData: FetchResultData) {
        storage.setString(Constants.TRANSACTION_ID, moveData.header?.transaksi_id.toString())
        storage.setString(Constants.MOVE_WAREHOUSE_FROM, moveData.header?.kode_warehouse_from!!)
        storage.setString(Constants.MOVE_SCAN_ID, moveData.header?.id.toString())
        storage.setString(Constants.MOVE_WAREHOUSE_FROM_ID, moveData.header?.warehouse_from_id!!)
        storage.setString(Constants.MOVE_WAREHOUSE_TO_ID, moveData.header?.warehouse_to_id!!)
        storage.setString(Constants.MOVE_WAREHOUSE_TO, moveData.header?.kode_warehouse_to!!)
        storage.setString(Constants.MOVE_LOCATOR_FROM_ID, moveData.detail.get(0)?.locator_from_id!!)
        storage.setString(Constants.MOVE_LOCATOR_FROM, moveData.detail.get(0)?.kode_locator_from!!)
        storage.setString(Constants.MOVE_LOCATOR_TO, moveData.detail.get(0)?.kode_locator_to!!)
        storage.setString(Constants.MOVE_LOCATOR_TO_ID, moveData.detail.get(0)?.locator_to_id!!)
    }

    val scanId: String get() = storage.getString(Constants.MOVE_SCAN_ID)
    val getTargetWHId: String get() = storage.getString(Constants.MOVE_WAREHOUSE_TO_ID)
    val getOriginLocatorId: String get() = storage.getString(Constants.MOVE_LOCATOR_FROM_ID)

    fun setScanId(scanId: String) {
        storage.setString(Constants.MOVE_SCAN_ID, scanId)

    }

    fun setTargetWHId(warehouseId: String, warehouseName: String) {
        storage.setString(Constants.MOVE_WAREHOUSE_TO_ID, warehouseId)
        storage.setString(Constants.MOVE_WAREHOUSE_TO, warehouseName)
    }

    fun setTargetLocatorId(locatorId: String, locatorName: String) {
        storage.setString(Constants.MOVE_LOCATOR_TO_ID, locatorId)
        storage.setString(Constants.MOVE_LOCATOR_TO, locatorName)
    }
    fun setOriginLocatorId(locatorId: String, locatorName: String) {
        storage.setString(Constants.MOVE_LOCATOR_FROM_ID, locatorId)
        storage.setString(Constants.MOVE_LOCATOR_FROM, locatorName)
    }


    fun setLocator(changeLocator: ChangeLocator) {
        storage.setString(Constants.MOVE_LOCATOR_TO, changeLocator.kode_locator)
        storage.setString(Constants.MOVE_LOCATOR_TO_ID, changeLocator.locator_id)
    }

}
