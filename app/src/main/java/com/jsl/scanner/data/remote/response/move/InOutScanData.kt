package com.jsl.scanner.data.remote.response.move

class InOutScanData(
    val part_list: MutableList<InOutPartData>,
    val scan_result: InOutPartScanData
) {
}