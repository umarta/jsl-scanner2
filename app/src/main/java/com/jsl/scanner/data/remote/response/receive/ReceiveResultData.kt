package com.jsl.scanner.data.remote.response.receive

class ReceiveResultData(
    val documentid: String? = null,
    val product: String? = null,
    val noitem: String? = null,
    val qtydo: String? = null,
    val qtyscan: String? = null,
    val qtysls: String? = null,
    val qtyerror: String? = null,
    val name: String? = null,
    val dateitemscan: String? = null,
    val qtylbh: String? = null,
    val m_locator_id: String? = null,
) {
}