package com.jsl.scanner.data.remote.response.event

class ItemData(
    var id: String,
    var event_id: String,
    var category_name: String,
    var priority: String,
    var featured_image: String) {
}