package com.jsl.scanner.data.remote.response

class IdNameData(
    var id: String,
    var name: String) {

    override fun toString(): String {
        return name
    }
}