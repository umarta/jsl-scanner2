package com.jsl.scanner.data.remote.response.event

class EventData(
    var id: String = "",
    var title: String = "",
    var sub_title: String = "",
    var slug: String = "",
    var start_date: String = "",
    var end_date: String = "",
    var status: Boolean = false,
    var get_item: MutableList<ItemData> = ArrayList()) {
}