package com.jsl.scanner.data.remote.response.move

class MoveHeaderData(
    val from_wh: String? = null,
    val to_wh: String? = null,
    val documentno: String? = null,
    val id: Int? = null,
) {
}