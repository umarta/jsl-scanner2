package com.jsl.scanner.data.remote

import com.jsl.scanner.data.remote.response.BaseResponse
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.auth.LoginData
import com.jsl.scanner.data.remote.response.auth.OtpData
import com.jsl.scanner.data.remote.response.move.*
import com.jsl.scanner.data.remote.response.receive.DocData
import com.jsl.scanner.data.remote.response.receive.PartListData
import com.jsl.scanner.data.remote.response.receive.ReceiveListData
import com.jsl.scanner.data.remote.response.receive.ReceiveResultData
import com.jsl.scanner.data.remote.response.scan.CheckResultData
import com.jsl.scanner.data.remote.response.scan.CheckStockData
import com.jsl.scanner.data.remote.response.scan.ScanResultData
import io.reactivex.Observable
import retrofit2.http.*
import java.util.*

interface MainService {
    @POST("auth/login")
    fun login(@Body request: HashMap<String, Any>): Observable<BaseResponse<LoginData>>

//    @Multipart
//    @POST("v1/auth/register")
//    fun register(@PartMap request: HashMap<String, Any>, @Part photoKtp: MultipartBody.Part,
//                     @Part photoSelfie: MultipartBody.Part): Observable<BaseResponse<Any>>

    @POST("v1/auth/register")
    fun register(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @POST("v1/auth/refresh_token")
    fun refreshToken(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @POST("v1/auth/request-otp")
    fun requestOtp(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @POST("v1/auth/validate-otp")
    fun validateOtp(@Body request: HashMap<String, Any>): Observable<BaseResponse<OtpData>>

    @GET("v1/territory/province")
    fun province(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("v1/territory/city")
    fun city(@Query("province_id") provinceID: String): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("v1/territory/district")
    fun district(@Query("city_id") cityID: String): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("v1/territory/sub-district")
    fun village(@Query("district_id") disctrictID: String): Observable<BaseResponse<MutableList<IdNameData>>>


    @POST("v1/auth/forgot-password")
    fun forgotPass(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>

    @POST("v1/auth/reset-password")
    fun resetPass(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>


    @POST("v1/user/change-password")
    fun changePass(@Body request: HashMap<String, Any>): Observable<BaseResponse<Any>>


    ///scanner

    @GET("master/warehouse")
    fun warehouse(): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("master/locator")
    fun locator(@Query("warehouse_id") warehouseId: String): Observable<BaseResponse<MutableList<IdNameData>>>

    @POST("master/locator")
    fun scanLocator(@Body request: HashMap<String, Any>): Observable<BaseResponse<IdNameData>>

    @GET("master/locator-slug")
    fun locatorSlug(@Query("warehouse_id") warehouseId: String): Observable<BaseResponse<MutableList<IdNameData>>>

    @POST("master/locator-slug")
    fun scanLocatorSlug(@Body request: HashMap<String, Any>): Observable<BaseResponse<IdNameData>>


    @POST("scan/opname")
    fun opName(@Body request: HashMap<String, Any>): Observable<BaseResponse<ScanResultData>>

    @POST("scan/check")
    fun checkOpname(@Body request: HashMap<String, Any>): Observable<BaseResponse<MutableList<CheckResultData>>>

    @POST("scan/move/check")
    fun fetchDoc(@Body request: HashMap<String, Any>): Observable<BaseResponse<FetchResultData>>

    @POST("scan/check-stock")
    fun checkStock(@Body request: HashMap<String, Any>): Observable<BaseResponse<MutableList<CheckResultData>>>

    @POST("scan/move/scan")
    fun scanProduct(@Body request: HashMap<String, Any>): Observable<BaseResponse<MutableList<MoveScanData>>>

    @POST("scan/move/assembly/penerimaan")
    fun scanPenerimaan(@Body request: HashMap<String, Any>): Observable<BaseResponse<MutableList<MoveScanData>>>

    @POST("scan/move/edit-locator")
    fun changeLocator(@Body request: HashMap<String, Any>): Observable<BaseResponse<ChangeLocator>>

    @GET("scan/move/active-list")
    fun activeList(
        @Query("warehouse_id") warehouseId: String,
        @Query("type") type: String

    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("scan/move/assembly/penerimaan/active")
    fun activeListPenerimaanAss(
        @Query("warehouse_id") warehouseId: String,
        @Query("type") type: String

    ): Observable<BaseResponse<MutableList<IdNameData>>>

    @GET("scan/move/active-list")
    fun selectedTrx(
        @Query("transaction_id") scanId: String,
        @Query("type") type: String
    ): Observable<BaseResponse<FetchResultData>>

    @GET("scan/move/assembly/penerimaan/detail")
    fun detailPenerimaanAss(
        @Query("transaction_id") scanId: String,
        @Query("type") type: String
    ): Observable<BaseResponse<MutableList<MoveScanData>>>

    @POST("scan/move/confirm")
    fun confirmTrx(
        @Query("transaction_id") scanId: String,
        @Query("type") type: String

    ): Observable<BaseResponse<Any>>

    @POST("scan/move/assembly/penerimaan/close")
    fun closeReceivingASS(
        @Query("transaction_id") scanId: String

    ): Observable<BaseResponse<Any>>

    @POST("scan/move/assembly")
    fun addNewTrx(
//        @Query("warehouse_to_id") warehouseTo: String,
//        @Query("warehouse_from_id") warehouseFrom: String
        @Body request: HashMap<String, Any>

    ): Observable<BaseResponse<AddNewTrx>>

    @GET("receive")
    fun receive(
        @Query("page") page: Int
    ): Observable<BaseResponse<MutableList<ReceiveListData>>>

    @GET("inout")
    fun inOutList(
        @Query("page") page: Int
    ): Observable<BaseResponse<MutableList<InOutData>>>

    @GET("receive/doc/{id}")
    fun docReceive(
        @Path("id") id: String
    ): Observable<BaseResponse<DocData>>

    @POST("receive/scan")
    fun receiveScan(@Body request: HashMap<String, Any>): Observable<BaseResponse<ReceiveResultData>>

    @POST("inout/act")
    fun scanInOut(@Body request: HashMap<String, Any>): Observable<BaseResponse<InOutScanData>>

    @POST("receive/check")
    fun docScan(@Body request: HashMap<String, Any>): Observable<BaseResponse<DocData>>

    @POST("inout/create-header")
    fun createHeaderMove(@Body request: HashMap<String, Any>): Observable<BaseResponse<MoveHeaderData>>

    @GET("inout/info/{id}")
    fun inOutPart(
        @Path("id") id: String
    ): Observable<BaseResponse<MutableList<InOutPartData>>>

}