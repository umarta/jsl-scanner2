package com.jsl.scanner.data.remote.response.receive

class PartListData(
    val noitem: Int,
    val product: String,
    val name: String,
    val qtydo: Int,
    val qtyscan: Int,
    val qtysls: Int,
    val documentid: String? = null
) {
}