//package com.jsl.scanner.data.local.room
//
//import androidx.room.Database
//import androidx.room.RoomDatabase
//import com.jsl.scanner.data.local.entity.EventEntity
//import com.jsl.scanner.data.local.entity.LastSeenEntity
//import com.jsl.scanner.data.local.entity.NotifyEntity
//import com.jsl.scanner.data.local.entity.SearchHistoryEntity
////import com.jsl.scanner.data.remote.response.banner.BannerData
////
////@Database(entities = [SearchHistoryEntity::class, LastSeenEntity::class,
////    BannerData::class, NotifyEntity::class], version = 1)
////abstract class AppDatabase : RoomDatabase() {
////    abstract fun searchDao(): SearchDao
////    abstract fun homeDao(): HomeDao
////    abstract fun notifyDao(): NotifyDao
////}