package com.jsl.scanner.data.user

import android.os.Bundle

class BundleBuilder {

    companion object {
        const val USER_ID = "user_id"
        const val USER_EMAIL = "user_email"
    }

    private val bundle = Bundle()

    fun put(key: String?, value: String?): BundleBuilder {
        bundle.putString(key, value)
        return this
    }

    fun build(): Bundle {
        val bundle = Bundle()
        bundle.putAll(this.bundle)
        return bundle
    }


}