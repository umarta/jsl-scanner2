package com.jsl.scanner.data.remote.response.move

class InOutPartScanData(
    val locator: String? = null,
    val product_code: String? = null,
    val product_name: String? = null,
    val sku: String? = null,
    val qty_from: String? = null,
    val qty_to: String? = null
) {
}