package com.jsl.scanner.data.remote.response.move

class InOutPartData(
    val product_code: String? = null,
    val product_name: String? = null,
    val sku: String? = null,
    val qty_from: Int? = null,
    val qty_to: Int? = null,

    ) {
}