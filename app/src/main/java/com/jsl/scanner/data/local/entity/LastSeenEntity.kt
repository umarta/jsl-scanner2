//package com.jsl.scanner.data.local.entity
//
//import androidx.room.ColumnInfo
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import java.util.*
//
//@Entity(tableName = "lastSeen")
//class LastSeenEntity(
//    @PrimaryKey
//    @ColumnInfo(name = "productID")
//    var productID: String,
//
//    @ColumnInfo(name = "userID")
//    var userID: String?,
//
//    @ColumnInfo(name = "productName")
//    var productName: String?,
//
//    @ColumnInfo(name = "productImage")
//    var productImage: String?,
//
//    @ColumnInfo(name = "count")
//    var count: Long,
//
//    @ColumnInfo(name = "status")
//    var status: String?,
//
//    @ColumnInfo(name = "date")
//    var date: Long?,
//
//    @ColumnInfo(name = "isDelete")
//    var isDelete: Boolean? = false
//
//
//)