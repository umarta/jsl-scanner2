package com.jsl.scanner.data.remote.response.move

class InOutData(

    val id: Int? = null,
    val documentno: String? = null,
    val document_date: String? = null,
    val from_wh: String? = null,
    val to_wh: String? = null,
    val status: String? = null,

    ) {
}