package com.jsl.scanner.data.remote.response.receive

class ReceiveData(
    val noitem: String? = null,
    val documentid: String? = null,
    val product: String? = null,
    val name: String? = null,
    val qtydo: String? = null,
    val dateitemscan: String? = null,
    val m_locator_id: String? = null,
) {
}