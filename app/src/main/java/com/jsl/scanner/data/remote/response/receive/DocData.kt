package com.jsl.scanner.data.remote.response.receive

class DocData(
    val documentid: String? = null,
    val m_warehouse_id: String? = null,
    val c_bpartner_id: String? = null,
    val list_do: MutableList<DoListData>,
    val list_part: MutableList<PartListData>
) {
}