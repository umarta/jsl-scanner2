package com.jsl.scanner.data.remote.response.scan

class CheckResultData(
    val kodeproduk: String? = null,
    val locator: String? = null,
    val warehouse: String? = null,
    val qty: String? = null,
    val namaproduk: String? = null,
    val sku: String? = null,

    ) {
}