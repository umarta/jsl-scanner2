package com.jsl.scanner.data.remote.response.move

class MoveScanData (
    var id: String,
    var scan_id: String,
    var no: String,
    var kode_produk: String,
    var locator_to_id: String,
    var kode_locator_to: String,
    var locator_from_id: String,
    var kode_locator_from: String,
    var qty_idem: String,
    var qty_scan: String,
    var product_name: String,
    var sku: String
)