//package com.jsl.scanner.data.local.entity
//
//import androidx.room.ColumnInfo
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import java.util.*
//
//@Entity(tableName = "notify")
//class NotifyEntity (
//    @PrimaryKey
//    @ColumnInfo(name = "id")
//    var id: String = UUID.randomUUID().toString(),
//
//    @ColumnInfo(name = "notifyID ")
//    var notifyID: String?,
//
//    @ColumnInfo(name = "itemID")
//    var itemID: String?,
//
//    @ColumnInfo(name = "userID")
//    var userID: String?,
//
//    @ColumnInfo(name = "message")
//    var message: String?,
//
//    @ColumnInfo(name = "subMessage")
//    var subMessage: String?,
//
//    @ColumnInfo(name = "image")
//    var image: String?,
//
//    @ColumnInfo(name = "deepLink")
//    var deepLink: String?,
//
//    @ColumnInfo(name = "type")
//    var type: String?,
//
//    @ColumnInfo(name = "status")
//    var status: String?,
//
//    @ColumnInfo(name = "date")
//    var date: Long?,
//
//    @ColumnInfo(name = "isDelete")
//    var isDelete: Boolean? = false,
//
//    @ColumnInfo(name = "isRead")
//    var isRead: Boolean? = false
//)