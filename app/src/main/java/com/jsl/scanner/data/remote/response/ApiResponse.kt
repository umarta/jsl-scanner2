package com.jsl.scanner.data.remote.response

import com.jsl.scanner.vo.StatusResponse
import com.jsl.scanner.vo.StatusResponse.SUCCESS
import com.jsl.scanner.vo.StatusResponse.ERROR

class ApiResponse<T>(val status: StatusResponse, val body: T, val message: String?) {
    companion object {
        fun <T> success(body: T): ApiResponse<T> = ApiResponse(SUCCESS, body, null)
        fun <T> error(msg: String, body: T): ApiResponse<T> = ApiResponse(ERROR, body, msg)
    }
}

