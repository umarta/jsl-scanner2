package com.jsl.scanner.data.remote

import androidx.lifecycle.MutableLiveData
import com.jsl.scanner.data.remote.response.BaseResponse
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.auth.LoginData
import com.jsl.scanner.data.remote.response.auth.OtpData
import com.jsl.scanner.data.remote.response.move.*
import com.jsl.scanner.data.remote.response.receive.DocData
import com.jsl.scanner.data.remote.response.receive.PartListData
import com.jsl.scanner.data.remote.response.receive.ReceiveListData
import com.jsl.scanner.data.remote.response.receive.ReceiveResultData
import com.jsl.scanner.data.remote.response.scan.CheckResultData
import com.jsl.scanner.data.remote.response.scan.CheckStockData
import com.jsl.scanner.data.remote.response.scan.ScanResultData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody


class MainRepository(private val mainService: MainService) {
    private val compositeDisposable = CompositeDisposable()
    val loading = MutableLiveData<Boolean>()

    fun login(req: HashMap<String, Any>): MutableLiveData<BaseResponse<LoginData>> {

        val data = MutableLiveData<BaseResponse<LoginData>>()
        loading.value = true
        mainService.login(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun register(
        request: HashMap<String, Any>, //photoKtp: MultipartBody.Part,
        photoSelfie: MultipartBody.Part
    ): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        mainService.register(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun refreshToken(tokenFcm: String): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
//        loading.value = true
        val req = HashMap<String, Any>()
        req["token"] = tokenFcm

        mainService.refreshToken(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
//                    loading.value = false
                }, {
                    it.printStackTrace()
//                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun province(query: String): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.province()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun city(
        provinceID: String,
        query: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.city(provinceID)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun districk(
        cityID: String,
        query: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.district(cityID)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun village(
        districkID: String,
        query: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.village(districkID)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun requestOtp(number: String): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        val request = HashMap<String, Any>()
        request["phone_number"] = number

        mainService.requestOtp(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun validateOtp(
        number: String,
        otpCode: String,
        type: String
    ): MutableLiveData<BaseResponse<OtpData>> {
        val data = MutableLiveData<BaseResponse<OtpData>>()
        loading.value = true
        val request = HashMap<String, Any>()
        request["phone_number"] = number
        request["otp_code"] = otpCode
        request["type"] = type

        mainService.validateOtp(request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun forgotPass(phone: String): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        val req = HashMap<String, Any>()
        req["phone_number"] = phone

        mainService.forgotPass(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun resetPass(
        phone: String,
        otpID: String,
        password: String,
        rePassword: String
    ): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        val req = HashMap<String, Any>()
        req["phone_number"] = phone
        req["otp_id"] = otpID
        req["password"] = password
        req["re_password"] = rePassword

        mainService.resetPass(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun changePass(
        oldPass: String,
        newPass: String,
        newRePass: String
    ): MutableLiveData<BaseResponse<Any>> {
        val data = MutableLiveData<BaseResponse<Any>>()
        loading.value = true
        val req = HashMap<String, Any>()
        req["old_password"] = oldPass
        req["new_password"] = newPass
        req["re_password"] = newRePass

        mainService.changePass(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun opName(
        warehouseId: String,
        locatorId: String,
        productCode: String,
        qtyAuto: Boolean,
        qtyOpName: String,
        isScan: Boolean
    ): MutableLiveData<BaseResponse<ScanResultData>> {
        val data = MutableLiveData<BaseResponse<ScanResultData>>()
        loading.value = true
        val req = HashMap<String, Any>()
        req["warehouse_id"] = warehouseId
        req["locator"] = locatorId
        req["product_code"] = productCode
        req["qty_auto"] = qtyAuto
        req["qty_opname"] = qtyOpName
        req["is_scan"] = isScan

        mainService.opName(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun checkOpname(
        warehouseId: String,
        productCode: String,
    ): MutableLiveData<BaseResponse<MutableList<CheckResultData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<CheckResultData>>>()
        loading.value = true
        val req = HashMap<String, Any>()
        req["warehouse_id"] = warehouseId
        req["product_code"] = productCode
        mainService.checkOpname(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun checkStock(
        warehouseId: String,
        productCode: String,
    ): MutableLiveData<BaseResponse<MutableList<CheckResultData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<CheckResultData>>>()
        loading.value = true
        val req = HashMap<String, Any>()
        req["warehouse_id"] = warehouseId
        req["product_code"] = productCode
        mainService.checkStock(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun receiveScan(
        formData: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<ReceiveResultData>> {
        val data = MutableLiveData<BaseResponse<ReceiveResultData>>()
        loading.value = true

        mainService.receiveScan(formData)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun scanInOut(
        formData: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<InOutScanData>> {
        val data = MutableLiveData<BaseResponse<InOutScanData>>()
        loading.value = true

        mainService.scanInOut(formData)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun docScan(
        formData: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<DocData>> {
        val data = MutableLiveData<BaseResponse<DocData>>()
        loading.value = true

        mainService.docScan(formData)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun createHeaderMove(
        formData: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<MoveHeaderData>> {
        val data = MutableLiveData<BaseResponse<MoveHeaderData>>()
        loading.value = true

        mainService.createHeaderMove(formData)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    private fun Disposable.addTo(disposable: CompositeDisposable) {
        disposable.add(this)
    }

    fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun warehouse(query: String): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.warehouse()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun locator(
        WarehouseID: String,
        query: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.locator(WarehouseID)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun scanLocator(
        WarehouseID: String,
        locatorName: String
    ): MutableLiveData<BaseResponse<IdNameData>> {
        val data = MutableLiveData<BaseResponse<IdNameData>>()
        val req = HashMap<String, Any>()
        req["warehouse_id"] = WarehouseID
        req["locator_id"] = locatorName

        loading.value = true
        mainService.scanLocator(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun locatorSlug(
        WarehouseID: String,
        query: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        loading.value = true
        mainService.locatorSlug(WarehouseID)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun inOutPart(
        inOutId: String,
    ): MutableLiveData<BaseResponse<MutableList<InOutPartData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<InOutPartData>>>()
        loading.value = true
        mainService.inOutPart(inOutId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun scanLocatorSlug(
        WarehouseID: String,
        locatorName: String
    ): MutableLiveData<BaseResponse<IdNameData>> {
        val data = MutableLiveData<BaseResponse<IdNameData>>()
        val req = HashMap<String, Any>()
        req["warehouse_id"] = WarehouseID
        req["locator_id"] = locatorName

        loading.value = true
        mainService.scanLocatorSlug(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun fetchDoc(
        docNo: String,
        transactionId: String,
        warehouseId: String,
        moveType: String
    ): MutableLiveData<BaseResponse<FetchResultData>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<FetchResultData>>()

        val req = HashMap<String, Any>()
        if (transactionId == "0") {
            req["type"] = "new"
        } else {
            req["type"] = "update"
            req["transaksi_id"] = transactionId
        }
        req["doc_no"] = docNo
        req["warehouse_id"] = warehouseId
        req["move_type"] = moveType

        mainService.fetchDoc(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun scanProduct(
        productNo: String,
        qty: String,
        isScan: Boolean,
        transactionId: String,
        type: String,
        locatorId: String
    ): MutableLiveData<BaseResponse<MutableList<MoveScanData>>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<MutableList<MoveScanData>>>()

        val req = HashMap<String, Any>()
        req["kode_produk"] = productNo
        req["qty"] = qty
        req["is_edit"] = isScan
        req["transaksi_id"] = transactionId
        req["type"] = type
        req["locator_id"] = locatorId
        mainService.scanProduct(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun scanPenerimaan(
        productNo: String,
        qty: String,
        isScan: Boolean,
        transactionId: String,
        type: String,
        locatorId: String
    ): MutableLiveData<BaseResponse<MutableList<MoveScanData>>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<MutableList<MoveScanData>>>()

        val req = HashMap<String, Any>()
        req["kode_produk"] = productNo
        req["qty"] = qty
        req["is_edit"] = isScan
        req["transaksi_id"] = transactionId
        req["type"] = type
        req["locator_id"] = locatorId
        mainService.scanPenerimaan(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun changeLocator(
        locator: String,
        scanId: String
    ): MutableLiveData<BaseResponse<ChangeLocator>> {
        loading.value = true

        val data = MutableLiveData<BaseResponse<ChangeLocator>>()

        val req = HashMap<String, Any>()
        req["kode_locator"] = locator
        req["scan_id"] = scanId
        mainService.changeLocator(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun activeList(
        warehouseId: String,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        mainService.activeList(warehouseId, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun activeListPenerimaanAss(
        warehouseId: String,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<MutableList<IdNameData>>>()
        mainService.activeListPenerimaanAss(warehouseId, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun selectedTrx(
        transactionId: String,
        type: String
    ): MutableLiveData<BaseResponse<FetchResultData>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<FetchResultData>>()
        mainService.selectedTrx(transactionId, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun detailPenerimaanAss(
        transactionId: String,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<MoveScanData>>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<MutableList<MoveScanData>>>()
        mainService.detailPenerimaanAss(transactionId, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun confirmTrx(transactionId: String, type: String): MutableLiveData<BaseResponse<Any>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<Any>>()
        mainService.confirmTrx(transactionId, type)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun closeReceivingASS(transactionId: String): MutableLiveData<BaseResponse<Any>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<Any>>()
        mainService.closeReceivingASS(transactionId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun addNewTrx(
        warehouseFromId: String,
        warehouseToId: String,
        originLocator: String
    ): MutableLiveData<BaseResponse<AddNewTrx>> {
        loading.value = true
        val data = MutableLiveData<BaseResponse<AddNewTrx>>()
        val req = HashMap<String, Any>()
        req["warehouse_to_id"] = warehouseToId
        req["warehouse_from_id"] = warehouseFromId
        req["kode_locator_from"] = originLocator
        mainService.addNewTrx(req)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun receiveList(page: Int): MutableLiveData<BaseResponse<MutableList<ReceiveListData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<ReceiveListData>>>()
        loading.value = true
        mainService.receive(page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }

    fun inOutList(page: Int): MutableLiveData<BaseResponse<MutableList<InOutData>>> {
        val data = MutableLiveData<BaseResponse<MutableList<InOutData>>>()
        loading.value = true
        mainService.inOutList(page)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


    fun docReceive(docNo: String): MutableLiveData<BaseResponse<DocData>> {
        val data = MutableLiveData<BaseResponse<DocData>>()
        loading.value = true
        mainService.docReceive(docNo)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    data.value = it
                    loading.value = false
                }, {
                    it.printStackTrace()
                    loading.value = false
                }
            ).addTo(disposable = compositeDisposable)
        return data
    }


}