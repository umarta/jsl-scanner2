package com.jsl.scanner.data.remote.response.move

class AddNewTrx(
    var id: String?,
    var doc_no: String?,
    var type_trx: String?,
    var doc_status: String?,
    var transaksi_id: String?,
    var warehouse_from_id: String?,
    var warehouse_to_id: String?,
    var kode_warehouse_from: String?,
    var kode_warehouse_to: String?
)