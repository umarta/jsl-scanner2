//package com.jsl.scanner.data.local.entity
//
//import androidx.room.ColumnInfo
//import androidx.room.Entity
//import androidx.room.PrimaryKey
//import com.jsl.scanner.data.remote.response.event.EventData
//import java.util.*
//
//@Entity(tableName = "event")
//class EventEntity (
//    @PrimaryKey
//    @ColumnInfo(name = "productID")
//    var productID: String = UUID.randomUUID().toString(),
//
//    @ColumnInfo(name = "eventData")
//    var eventData: String)