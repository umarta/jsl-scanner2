package com.jsl.scanner.data.remote.response.scan

import java.io.Serializable

class ScanResultData(
    var wh: String,
    var locator: String,
    var kodeproduk: String,
    var nama: String,
    var qty: String,
    var tglopname: String,
    var sku: String,
    var stok: String

)