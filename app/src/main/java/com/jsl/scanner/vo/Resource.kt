package com.jsl.scanner.vo
import com.jsl.scanner.vo.Status.SUCCESS
import com.jsl.scanner.vo.Status.ERROR
import com.jsl.scanner.vo.Status.LOADING


data class Resource<T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> = Resource(SUCCESS, data, null)

        fun <T> error(data: T?, msg: String?): Resource<T> = Resource(ERROR, data, msg)

        fun <T> loading(data: T?): Resource<T> = Resource(LOADING, data, null)
    }
}
