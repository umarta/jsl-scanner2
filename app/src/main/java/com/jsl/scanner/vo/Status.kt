package com.jsl.scanner.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
