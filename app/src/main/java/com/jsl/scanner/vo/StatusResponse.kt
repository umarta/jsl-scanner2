package com.jsl.scanner.vo

enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}
