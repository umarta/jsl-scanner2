package com.jsl.scanner.ui.move

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityActiveTransactionBinding
import com.jsl.scanner.ui.home.HomeActivity
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.Constants
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class ActiveTransactionActivity : AppCompatActivity(), BaseApp.Listener,
    ActiveTransactionAdapter.Listener {

    @Inject
    lateinit var userManager: UserManager
    private lateinit var activeTransactionAdapter: ActiveTransactionAdapter
    private val viewModel: InventoryMoveViewModel by viewModels()
    private lateinit var moveType: String

    private lateinit var binding: ActivityActiveTransactionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityActiveTransactionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.tvTitle.text = userManager.warehouseName
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra(Constants.MOVE_TYPE)) {
            moveType = intent.getStringExtra(Constants.MOVE_TYPE).toString()
        }
    }

    override fun setOnClick() {
        binding.ivAdd.setOnClickListener {
            val i = Intent(this, InventoryMoveActivity::class.java)
            i.putExtra(Constants.TRANSACTION_ID, "0")
            i.putExtra(Constants.MOVE_TYPE, moveType)
            this.startActivity(i)
        }
        binding.ivBack.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }

    override fun setAdapter() {
        activeTransactionAdapter = ActiveTransactionAdapter(this, ArrayList(), this, moveType)
        binding.rvActiveTransaction.adapter = activeTransactionAdapter
        binding.rvActiveTransaction.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
    }

    override fun loadData() {
        viewModel.activeList(userManager.warehouseId, moveType).observe(this, Observer {
            if (it.success!!) {
                activeTransactionAdapter.setItems(it.data)
            }
        })
    }

    override fun onActiveTrx(data: IdNameData) {
        finish()

    }

}