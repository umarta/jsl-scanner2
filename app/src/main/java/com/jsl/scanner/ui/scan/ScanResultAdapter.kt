package com.jsl.scanner.ui.scan

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.scan.ScanResultData
import com.jsl.scanner.databinding.ScanResultItemBinding

class ScanResultAdapter(
    private val activity: Activity,
    private var data: MutableList<ScanResultData>,
    val listener: Listener
) : RecyclerView.Adapter<ScanResultAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScanResultAdapter.Holder {
        val binding =
            ScanResultItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ScanResultAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    inner class Holder(
        private val binding: ScanResultItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(mData: ScanResultData, position: Int, listener: Listener) {
            with(binding) {
                tvProductName.text = mData.nama
                tvProductCode.text = mData.kodeproduk
//                    "${mData.address}, ${mData.get_village.name}, ${mData.get_village.get_district.name}" +
//                        ", ${mData.get_village.get_district.get_city.name}, ${mData.get_village.get_district.get_city.get_province.name}" +
//                        ", ${mData.postal_code}"
                tvQtyOnHand.text = mData.stok
                tvQtyOpname.text = mData.qty
                itemView.setOnClickListener { listener.onAddress(mData) }
            }
        }
    }

    interface Listener {
        fun onAddress(data: ScanResultData)
    }

}



