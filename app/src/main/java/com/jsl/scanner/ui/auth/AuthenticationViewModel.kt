package com.jsl.scanner.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.scanner.data.remote.MainRepository
import com.jsl.scanner.data.remote.MainService
import com.jsl.scanner.data.remote.response.BaseResponse
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.auth.LoginData
import com.jsl.scanner.data.remote.response.auth.OtpData
import com.jsl.scanner.model.PersonModel
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class AuthenticationViewModel @Inject constructor(mainService: MainService) : ViewModel() {
    private val repo = MainRepository(mainService)
    private val _page = MutableLiveData<Int>()
    private val _person = MutableLiveData<PersonModel>()

    val loading: LiveData<Boolean> get() = repo.loading
    val page: LiveData<Int> get() = _page
    val person: LiveData<PersonModel> get() = _person

    fun setPage(int: Int) {
        _page.value = int
    }

    fun setPerson(person: PersonModel) {
        _person.value = person
    }

    fun login(req: HashMap<String, Any>): LiveData<BaseResponse<LoginData>> = repo.login(req)
    fun register(
        request: HashMap<String, Any>, //photoKtp: MultipartBody.Part,
        photoSelfie: MultipartBody.Part
    ): LiveData<BaseResponse<Any>> = repo.register(request, /*photoKtp,*/ photoSelfie)

    fun refreshToken(tokenFcm: String): LiveData<BaseResponse<Any>> = repo.refreshToken(tokenFcm)
    fun province(query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.province(query)

    fun city(provinceID: String, query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.city(provinceID, query)

    fun districk(cityID: String, query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repo.districk(cityID, query)

    fun village(
        districkID: String,
        query: String
    ): LiveData<BaseResponse<MutableList<IdNameData>>> = repo.village(districkID, query)

    fun requestOtp(number: String): LiveData<BaseResponse<Any>> = repo.requestOtp(number)
    fun validateOtp(
        number: String,
        otpCode: String,
        type: String
    ): LiveData<BaseResponse<OtpData>> = repo.validateOtp(number, otpCode, type)

    fun forgotPass(phone: String): MutableLiveData<BaseResponse<Any>> = repo.forgotPass(phone)
    fun resetPass(
        phone: String,
        otpID: String,
        password: String,
        rePassword: String
    ): MutableLiveData<BaseResponse<Any>> = repo.resetPass(phone, otpID, password, rePassword)

    override fun onCleared() {
        super.onCleared()
        repo.onDestroy()
    }

}