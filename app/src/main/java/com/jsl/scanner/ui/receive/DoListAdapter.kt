package com.jsl.scanner.ui.receive

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.receive.DoListData
import com.jsl.scanner.databinding.ViewDoListBinding

class DoListAdapter(
    private val activity: Activity,
    private var data: MutableList<DoListData>,
    val listener: DoListAdapter.Listener,
) : RecyclerView.Adapter<DoListAdapter.Holder>() {
    val res = HashMap<String, Any>()

    fun setItems(data: MutableList<DoListData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<DoListData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = ViewDoListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(
        private val binding: ViewDoListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(mData: DoListData, position: Int, listener: Listener) {
            with(binding) {
                tvKodeDo.text = mData.documentno
            }
        }
    }

    interface Listener {
        fun onLoadBom(data: DoListData)
    }

}