package com.jsl.scanner.ui.assembling.active

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.move.MoveManager
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.BsheetNewDataDialogBinding
import com.jsl.scanner.ui.move.InventoryMoveViewModel
import com.jsl.scanner.utils.hide
import javax.inject.Inject

class WarehouseSheet(
    val listener: Listener,
    private var warehouseList: MutableList<IdNameData>?,
    private var locatorList: MutableList<IdNameData>?,
    private var warehouseData: IdNameData?,
    private var locatorData: IdNameData?
) :
    BottomSheetDialogFragment() {
    @Inject
    lateinit var userManager: UserManager

    @Inject
    lateinit var moveManager: MoveManager

    private lateinit var binding: BsheetNewDataDialogBinding

    private val viewModel: InventoryMoveViewModel by viewModels()
    private lateinit var spWarehouse: MaterialSpinner
    private lateinit var spLocator: MaterialSpinner
    private lateinit var progressBar: ProgressBar

    fun setWarehouse(warehouse: MutableList<IdNameData>) {
        warehouseList = warehouse
        val subBrandAdapter = MaterialSpinnerAdapter<IdNameData>(context, warehouseList)
        spWarehouse.setAdapter(subBrandAdapter)
        progressBar.hide()
    }

    fun setLocator(locator: MutableList<IdNameData>) {
        locatorList = locator
        val subBrandAdapter = MaterialSpinnerAdapter<IdNameData>(context, locatorList)

        spLocator.setAdapter(subBrandAdapter)
        progressBar.hide()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = BsheetNewDataDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            spWarehouse.setOnItemSelectedListener(MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                warehouseData = item
                listener.onWarehouse(item)
            })

            val brandAdapter = MaterialSpinnerAdapter<IdNameData>(context, warehouseList)
            spWarehouse.setAdapter(brandAdapter)
            tvAction.setOnClickListener {
                listener.onAdd()
                dismiss()
            }
        }
    }

    private fun addNew() {

    }

    interface Listener {
        fun onWarehouse(warehouse: IdNameData)
        fun onAdd()
    }

}