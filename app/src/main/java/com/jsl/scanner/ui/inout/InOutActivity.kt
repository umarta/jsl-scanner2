package com.jsl.scanner.ui.inout

import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.move.InOutPartData
import com.jsl.scanner.databinding.ActivityInOutBinding
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InOutActivity : AppCompatActivity(), BaseApp.Listener, InOutPartAdapter.Listener {
    var moveId: String = ""
    var wh_id: String = ""
    private val viewModel: ScanViewModel by viewModels()
    val loading2 = MutableLiveData<Boolean>()
    private lateinit var inOutPartAdapter: InOutPartAdapter

    private var mediaPlayer: MediaPlayer? = null
    private var mediaPlayerFail: MediaPlayer? = null
    private var locaorScanName: String? = null
    private lateinit var locator: IdNameData
    private var isAuto: Boolean = true
    private var qty: String = "1"
    private var type: String = "out"

    private lateinit var binding: ActivityInOutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInOutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("move_id")) {
            moveId = intent.getStringExtra("move_id").toString()
            wh_id = intent.getStringExtra("wh_from").toString()
        }
    }

    override fun setOnClick() {
        binding.apply {
            inputLocatorValue.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    locaorScanName = inputLocatorValue.text.toString()

                    scanLocator()

                }
                false
            })
            spLocator.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    locator = item
                    viewModel.setLocator(locator)
                    inputDocumentCode.requestFocus()
                }
            )
            viewModel.locator.observe(this@InOutActivity, Observer {
                locator = it
            })
            inputDocumentCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    scanProduct()
                }
                false
            })
            autoGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
                val rb: RadioButton = findViewById<View>(checkedId) as RadioButton

                if (rb.getText().toString() == "Tidak") {
                    tlInputQty.show()
                    isAuto = false

                } else {
                    tlInputQty.hide()
                    isAuto = true

                }
            })
            rbScanType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
                val rb: RadioButton = findViewById<View>(checkedId) as RadioButton

                if (rb.getText().toString() == "Out") {
                    type = "out"

                } else {
                    tlInputQty.hide()
                    type = "in"

                }
            })
        }


    }

    override fun setAdapter() {
        inOutPartAdapter = InOutPartAdapter(this, ArrayList(), this)

        binding.rvPartBomList.adapter = inOutPartAdapter
        binding.rvPartBomList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


    }

    override fun setContent() {
        getLocator(wh_id, "")
        inOutPart()
    }

    override fun loadData() {
    }

    fun scanProduct() {
        val product_code = binding.inputDocumentCode.text.toString()
        val req = HashMap<String, Any>()
        if (!isAuto) {
            qty = binding.inputQty.text.toString()
        }
        req["move_id"] = moveId
        req["product_code"] = product_code
        req["locator"] = locator.id
        req["type"] = type
        req["qty"] = qty
        viewModel.scanInOut(req).observe(this, Observer {
            binding.apply {
                if (it.success == true) {
                    cardResult.show()
                    tvProductName.text = it.data.scan_result.product_name
                    tvProductCode.text = it.data.scan_result.product_code
                    tvQtyOut.text = it.data.scan_result.qty_from
                    tvQtyIn.text = it.data.scan_result.qty_to

                    tvSku.text = it.data.scan_result.sku
                    inputDocumentCode.text?.clear()
                    inputQty.text?.clear()
                    inputDocumentCode.isEnabled = true
                    inputDocumentCode.requestFocus()

                    inOutPartAdapter.clear()
                    inOutPartAdapter.setItems(it.data.part_list)
                    mediaPlayer?.start()
                } else {
                    inputDocumentCode.isEnabled = true
                    inputDocumentCode.text?.clear()

                    mediaPlayerFail?.start()
                }
            }
        })
    }

    private fun getLocator(WarehouseId: String, query: String) {
        viewModel.locatorSlug(WarehouseId, query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spLocator.setAdapter(spinnerAdapter)
            }
        })
    }

    private fun inOutPart() {
        viewModel.inOutPart(moveId).observe(this, Observer {
            if (it.success!!) {
                toast("test")
                inOutPartAdapter.setItems(it.data)
            }
        })
    }


    fun scanLocator() {
        viewModel.scanLocatorSlug(wh_id, locaorScanName.toString()).observe(this, Observer {
            if (it.success!!) {
                mediaPlayer?.start()
                locator = it.data
                binding.spLocator.setText(locator.name)
                viewModel.setLocator(locator)

                binding.inputProductCode.requestFocus()
            } else {
                mediaPlayerFail?.start()
                binding.inputLocatorValue.requestFocus()
                toast(it.message?.get(0))
            }
        })
    }

    override fun onLoadBom(data: InOutPartData) {
    }


}