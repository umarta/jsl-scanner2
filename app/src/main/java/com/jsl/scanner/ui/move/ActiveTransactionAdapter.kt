package com.jsl.scanner.ui.move

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.databinding.ViewItemActiveTransactionBinding
import com.jsl.scanner.utils.Constants

class ActiveTransactionAdapter(
    private val activity: Activity,
    private var data: MutableList<IdNameData>,
    val listener: Listener,
    private val moveType: String
) : RecyclerView.Adapter<ActiveTransactionAdapter.Holder>() {

    fun setItems(data: MutableList<IdNameData>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = ViewItemActiveTransactionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return Holder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    inner class Holder(
        private val binding: ViewItemActiveTransactionBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(mData: IdNameData, position: Int, listener: Listener) {
            with(binding) {
                tvTrxName.text = mData.name
                itemView.setOnClickListener {
                    val i = Intent(activity, InventoryMoveActivity::class.java)
                    i.putExtra(Constants.TRANSACTION_ID, mData.id)
                    i.putExtra(Constants.MOVE_TYPE, moveType)
                    activity.startActivity(i)
                }
            }
        }
    }

    interface Listener {
        fun onActiveTrx(data: IdNameData)
    }
}