package com.jsl.scanner.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.databinding.DialogAddNewTransactionBinding
import com.jsl.scanner.utils.hide

class AddNewDataDialog(
    val message: String,
    val listener: Listener,
    private var warehouseList: MutableList<IdNameData>
) : DialogFragment() {

    private lateinit var binding: DialogAddNewTransactionBinding
    private lateinit var spWarehouse: MaterialSpinner
    private lateinit var progressBar: ProgressBar

    fun setWarehouse(whData: MutableList<IdNameData>) {
        warehouseList = whData
        val subBrandAdapter = MaterialSpinnerAdapter<IdNameData>(context, warehouseList)
        spWarehouse.setAdapter(subBrandAdapter)
        progressBar.hide()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(R.color.transparent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        binding = DialogAddNewTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvText.text = message

        binding.tvCancel.setOnClickListener { dismiss() }
        binding.tvAction.setOnClickListener {
            listener.onAddNewDataDialog()
            dismiss()
        }


    }

    interface Listener {
        fun onAddNewDataDialog()
    }

}