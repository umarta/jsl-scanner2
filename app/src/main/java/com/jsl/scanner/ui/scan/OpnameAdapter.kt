package com.jsl.scanner.ui.scan

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.scan.CheckResultData
import com.jsl.scanner.databinding.ViewOpnameListBinding
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show

class OpnameAdapter(
    private val activity: Activity,
    private var data: MutableList<CheckResultData>,
    val listener: Listener,
    val type: String


) : RecyclerView.Adapter<OpnameAdapter.Holder>() {
    val res = HashMap<String, Any>()

    fun setItems(data: MutableList<CheckResultData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<CheckResultData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding =
            ViewOpnameListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(
        private val binding: ViewOpnameListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(mData: CheckResultData, position: Int, listener: Listener) {
            with(binding) {
                if (type != "check_stock") {
                    tvKodePart.setText(mData.kodeproduk)
                    tvLocator.setText(mData.locator.toString())
                    tvQty.setText(mData.qty.toString())
                    if (mData.warehouse != null) {
                        tvWarehouse.setText(mData.warehouse.toString())
                        tvWarehouse.show()
                    }
                } else {
                    tvKodePart.hide()
                    tvLocator.setText(mData.locator.toString())
                    tvQty.setText(mData.qty.toString())
                    tvWarehouse.text = mData.warehouse ?: ""
//                    tv_locator.textSize = 32F
//                    tv_qty.textSize = 32F

                }
            }
        }
    }

    interface Listener {
        fun onLoadBom(data: CheckResultData)
    }

}