package com.jsl.scanner.ui.inout

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.move.InOutPartData
import com.jsl.scanner.databinding.ViewInoutPartListBinding

class InOutPartAdapter(
    private val activity: Activity,
    private var data: MutableList<InOutPartData>,
    val listener: InOutPartAdapter.Listener,
) : RecyclerView.Adapter<InOutPartAdapter.Holder>() {
    val res = HashMap<String, Any>()

    fun setItems(data: MutableList<InOutPartData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<InOutPartData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding =
            ViewInoutPartListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(
        private val binding: ViewInoutPartListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(mData: InOutPartData, position: Int, listener: Listener) {
            with(binding) {
                var qty_in: String? = null
                qty_in = if (mData.qty_to == null) {
                    "0"
                } else {
                    mData.qty_to.toString()
                }
//                val qty_in =  mData.qty_to.toString()
                tvProductCode.text = "Kode Produk : " + mData.product_code
                tvQtyOut.text = "QTY OUT : " + mData.qty_from.toString()
                tvQtyIn.text = "QTY IN : $qty_in"
                tvProductName.text = "Product Name : " + mData.product_name
                tvSku.text = "Product SKU : " + mData.sku
            }
        }
    }

    interface Listener {
        fun onLoadBom(data: InOutPartData)
    }

}