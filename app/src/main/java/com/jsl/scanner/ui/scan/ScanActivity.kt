package com.jsl.scanner.ui.scan

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.integration.android.IntentIntegrator
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.scan.CheckResultData
import com.jsl.scanner.databinding.ActivityScanBinding
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.text.DecimalFormat


@AndroidEntryPoint
class ScanActivity : AppCompatActivity(), OpnameAdapter.Listener {

    private val viewModel: ScanViewModel by viewModels()
    val loading2 = MutableLiveData<Boolean>()
    private var qrScanIntegrator: IntentIntegrator? = null

    private var mediaPlayer: MediaPlayer? = null
    private var mediaPlayerFail: MediaPlayer? = null
    private var enter: Boolean = false
    private var enterLocator: Boolean = false
    private var locaorScanName: String? = null
    private var scanOrLocator: Boolean = false
    private lateinit var wh: IdNameData
    private lateinit var locator: IdNameData
    private lateinit var opnameAdapter: OpnameAdapter

    private lateinit var binding: ActivityScanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        super.onCreate(savedInstanceState)

        binding = ActivityScanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        warehouse("")
        mediaPlayer = MediaPlayer.create(this, R.raw.okay)
        mediaPlayerFail = MediaPlayer.create(this, R.raw.no)
        setupScanner()
        opnameAdapter = OpnameAdapter(this, ArrayList(), this, "scan")
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.rvPartBomList.adapter = opnameAdapter
        binding.rvPartBomList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.autoGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            val rb: RadioButton = findViewById<View>(checkedId) as RadioButton

            if (rb.getText().toString() == "Tidak") {
                binding.tlInputQty.show()
//                tv_kalkulator.show()
                binding.rlKalkulator.show()
                binding.tvSave.show()
                (getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
                    .showSoftInput(binding.inputQty, InputMethodManager.SHOW_IMPLICIT)

            } else {
                binding.ctaBottom.hide()
                binding.tlInputQty.hide()
                binding.tvSave.hide()

//                tv_kalkulator.hide()
                binding.rlKalkulator.hide()
                binding.inputQty.setText("1")
            }
        })

        binding.rbScanType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            val rb: RadioButton = findViewById<View>(checkedId) as RadioButton

            if (rb.getText().toString() == "Check") {
                binding.TILAutoGroup.hide()
                binding.inputQty.setText("0")
                binding.rlKalkulator.hide()
                binding.tlInputQty.hide()
                binding.rvOpnameList.show()
                binding.llCheck.show()
                binding.cardResult.hide()
                binding.tvSave.hide()
            } else {
                binding.TILAutoGroup.show()
                binding.rvOpnameList.hide()
                binding.llCheck.hide()
                binding.cardResult.hide()

                val auto = binding.autoGroup.checkedRadioButtonId // R.id.male, int value 2131361895
                val autoString = resources.getResourceEntryName(auto) // "male"

                if (autoString.toString() == "aq_tidak") {
                    binding.tlInputQty.show()
//                tv_kalkulator.show()
                    binding.rlKalkulator.show()
                    binding.tvSave.show()
                    (getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
                        .showSoftInput(binding.inputQty, InputMethodManager.SHOW_IMPLICIT)
                }

            }
            binding.inputProductCode.requestFocus()
        })

        binding.inputProductCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                autoQty()
                enter = true
            }
            false
        })

        binding.inputLocatorValue.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                locaorScanName = binding.inputLocatorValue.text.toString()
                enterLocator = true

                scanLocator()

            }
            false
        })




        binding.tvCount.setOnClickListener {

            binding.apply {
                if (!x1.text.isNullOrEmpty() && !x2.text.isNullOrEmpty()) {
                    val x1 = x1.text.toString().toDouble()
                    val x2 = x2.text.toString().toDouble()
                    val s = if (x3.text.isNullOrEmpty()) 0 else Integer.parseInt(x3.text.toString())
                    val result = (x1 * x2) + s
                    val df = DecimalFormat("#.###")
                    toast(result.toString())
                    inputQty.setText(df.format(result))
                } else {
                    toast("value X tidak boleh kosong")
                    inputQty.text?.clear()
                }
            }


//            Log.d(result.toString())
//            input_qty.setText(Integer.parseInt(result))
        }
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
        binding.tvSave.setOnClickListener {
            scan()
            binding.inputQty.text?.clear()
        }
        binding.spWarehouse.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                wh = item
                getLocator(wh.id, "")
                viewModel.setWarehouse(wh)

            }
        )
        viewModel.warehouse.observe(this, Observer {
            wh = it

        })
        viewModel.scanOrLocator.observe(this, Observer {
            scanOrLocator = it
        })


        binding.btnScan.setOnClickListener {
            scanOrLocator = true
            viewModel.setScanOrLocator(scanOrLocator)
            performAction()
        }

        binding.btnScanLocator.setOnClickListener {
            scanOrLocator = false
            viewModel.setScanOrLocator(scanOrLocator)
            performAction()

        }

        binding.spLocator.setOnItemSelectedListener(
            MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                locator = item
                viewModel.setLocator(locator)
                binding.inputProductCode.requestFocus()
            }
        )
        viewModel.locator.observe(this, Observer {
            locator = it
        })

    }

    fun autoQty() {
        val autoQtyRB = binding.autoGroup.checkedRadioButtonId
        val autoQtyString =
            resources.getResourceEntryName(autoQtyRB).toString() == "aq_ya"

        val scanTypeRb = binding.rbScanType.checkedRadioButtonId
        val scanTypeString =
            resources.getResourceEntryName(scanTypeRb).toString() == "rb_check_act"

        if (autoQtyString) {
            scan()
        } else {
            if (scanTypeString) {
                scan()

            } else {

                binding.tlInputQty.requestFocus()
                (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(
                    InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
                );
            }
        }
    }

    //    fun check() {
//        var warehouseId = wh.id
//        var locatorId = locator.id
//        var productCode = input_product_code.text
//
//
//
//    }
    private fun setupScanner() {
        qrScanIntegrator = IntentIntegrator(this)
        qrScanIntegrator?.setOrientationLocked(true)
    }

    @SuppressLint("SetTextI18n")
    fun scan() {
        binding.progressBar.show()
        var warehouseId = "-";
        var locatorId = "-"

        if (this::wh.isInitialized) {
            warehouseId = wh.id
        } else {
            toast("Pilih warehouse terlebih dahulu")
            binding.progressBar.hide()
            mediaPlayerFail?.start()

            return
        }
        if (this::locator.isInitialized) {
            locatorId = locator.id
        } else {
            toast("Pilih locator terlebih dahulu")
            binding.progressBar.hide()
            mediaPlayerFail?.start()

            return
        }

        val productCode = binding.inputProductCode.text
        val qty = binding.inputQty.text
        val autoQtyRB = binding.autoGroup.checkedRadioButtonId // R.id.male, int value 2131361895
        val autoQtyString =
            resources.getResourceEntryName(autoQtyRB).toString() == "aq_ya"   // "male"
        val isScan = binding.rbScanType.checkedRadioButtonId // R.id.male, int value 2131361895
        val isScanString =
            resources.getResourceEntryName(isScan).toString() == "rb_scan_act"   // "male"
        loading2.value = true
        binding.inputProductCode.isEnabled = false
        if (isScanString) {
            viewModel.opName(
                warehouseId,
                locatorId,
                productCode.toString(),
                autoQtyString,
                qty.toString(),
                isScanString
            ).observe(this, Observer {
                binding.apply {
                    if (it.success!!) {
                        inputProductCode.isEnabled = true

                        cardResult.show()
                        LlQty.show()
                        cardResult.show()

                        tvProductName.text = it.data.nama
                        tvProductCode.text = it.data.kodeproduk
                        tvQtyOnHand.text = it.data.stok
                        tvQtyOpname.text = it.data.qty
                        tvSku.text = it.data.sku
                        inputProductCode.text?.clear()
                        mediaPlayer?.start()
                        inputProductCode.requestFocus()
                        progressBar.hide()
                        if (!enter) {
                            performAction()
                        }

                        inputProductCode.requestFocus()

                    } else {
                        toast("Product tidak ditemukan")
                        inputProductCode.isEnabled = true

                        mediaPlayerFail?.start()
                        tvProductName.text = "Product tidak ditemukan"
                        tvProductCode.text = productCode.toString()
                        inputProductCode.text?.clear()
                        cardResult.show()
                        LlQty.hide()
                        inputProductCode.requestFocus()
                        progressBar.hide()
                    }
                }
            })
        } else {
            viewModel.checkProduct(
                warehouseId,
                productCode.toString(),
            ).observe(this, Observer {
                binding.apply {
                    if (it.success!!) {
                        inputProductCode.isEnabled = true
                        val data = it.data
                        cardResult.hide()
                        LlQty.hide()

                        opnameAdapter.setItems(data)
                    } else {
                        toast("Product tidak ditemukan")
                        inputProductCode.isEnabled = true

                        mediaPlayerFail?.start()
                        tvProductName.text = "Product tidak ditemukan"
                        tvProductCode.text = productCode.toString()
                        inputProductCode.text?.clear()
                        cardResult.show()
                        LlQty.hide()
                        inputProductCode.requestFocus()
                        progressBar.hide()

                    }
                    progressBar.hide()
                }

            })
        }

    }

    private fun performAction() {
        enter = false
        enterLocator = false

        // Code to perform action when button is clicked.
        qrScanIntegrator?.initiateScan()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            // If QRCode has no data.
            if (result.contents == null) {
                Toast.makeText(this, getString(R.string.result_not_found), Toast.LENGTH_LONG).show()
            } else {
                if (scanOrLocator) {
                    binding.inputProductCode.setText(result.contents)
                    autoQty()
                } else {
                    binding.inputLocatorValue.setText(result.contents)
                    locaorScanName = result.contents
                    scanLocator()
                }
                // If QRCode contains data.
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun scanLocator() {
        viewModel.scanLocator(wh.id, locaorScanName.toString()).observe(this, Observer {
            if (it.success!!) {
                mediaPlayer?.start()
                locator = it.data
                binding.spLocator.setText(locator.name)
                binding.inputProductCode.requestFocus()
            } else {
                mediaPlayerFail?.start()
                binding.inputLocatorValue.requestFocus()
                toast(it.message?.get(0))
            }
        })

    }

    private fun warehouse(query: String) {
        viewModel.warehouse(query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spWarehouse.setAdapter(spinnerAdapter)
            }
        })
    }

    private fun getLocator(WarehouseId: String, query: String) {
        viewModel.locator(WarehouseId, query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spLocator.setAdapter(spinnerAdapter)
                binding.rlLocator.show()
                binding.llScanLocator.show()
            }
        })
    }

    override fun onLoadBom(data: CheckResultData) {
        finish()

    }


}