package com.jsl.scanner.ui.inout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.scanner.data.remote.MainRepository
import com.jsl.scanner.data.remote.MainService
import com.jsl.scanner.data.remote.response.BaseResponse
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.receive.DoListData
import com.jsl.scanner.data.remote.response.receive.DocData
import com.jsl.scanner.data.remote.response.receive.PartListData
import com.jsl.scanner.data.remote.response.receive.ReceiveListData
import com.jsl.scanner.data.remote.response.receive.ReceiveResultData
import com.jsl.scanner.data.remote.response.scan.CheckResultData
import com.jsl.scanner.data.remote.response.scan.ScanResultData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InOutViewModel @Inject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading

    fun warehouse(query: String): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.warehouse(query)

    fun locator(
        warehouseId: String,
        query: String
    ): LiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.locator(warehouseId, query)

    fun scanLocator(
        warehouseId: String,
        locatorName: String
    ): LiveData<BaseResponse<IdNameData>> =
        repoMain.scanLocator(warehouseId, locatorName)

    fun opName(
        warehouseId: String,
        locatorId: String,
        productCode: String,
        qtyAuto: Boolean,
        qtyOpName: String,
        isScan: Boolean
    ): MutableLiveData<BaseResponse<ScanResultData>> =
        repoMain.opName(warehouseId, locatorId, productCode, qtyAuto, qtyOpName, isScan)

    fun checkProduct(
        warehouseId: String,
        productCode: String,
    ): MutableLiveData<BaseResponse<MutableList<CheckResultData>>> =
        repoMain.checkOpname(warehouseId, productCode)

    fun checkStock(
        warehouseId: String,
        productCode: String,
    ): MutableLiveData<BaseResponse<MutableList<CheckResultData>>> =
        repoMain.checkStock(warehouseId, productCode)

    fun receiveScan(
        data: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<ReceiveResultData>> =
        repoMain.receiveScan(data)

    fun docScan(
        data: HashMap<String, Any>
    ): MutableLiveData<BaseResponse<DocData>> =
        repoMain.docScan(data)


    private val _warehouse = MutableLiveData<IdNameData>()
    val warehouse: LiveData<IdNameData> get() = _warehouse

    private val _locator = MutableLiveData<IdNameData>()
    val locator: LiveData<IdNameData> get() = _locator

    private val _scan_or_locator = MutableLiveData<Boolean>()
    val scanOrLocator: LiveData<Boolean> get() = _scan_or_locator

    private val _doc_list = MutableLiveData<MutableList<DoListData>>()
    val docList: LiveData<MutableList<DoListData>> get() = _doc_list

    fun setDocList(docListData: MutableList<DoListData>) {
        _doc_list.value = docListData
    }


    private val _part_list = MutableLiveData<MutableList<PartListData>>()
    val partList: LiveData<MutableList<PartListData>> get() = _part_list

    fun setPartList(partListData: MutableList<PartListData>) {
        _part_list.value = partListData
    }


    fun setWarehouse(warehouse: IdNameData) {
        _warehouse.value = warehouse
    }

    fun setLocator(locator: IdNameData) {
        _locator.value = locator
    }

    fun setScanOrLocator(scanOrLocator: Boolean) {
        _scan_or_locator.value = scanOrLocator
    }

    fun receiveList(page: Int): MutableLiveData<BaseResponse<MutableList<ReceiveListData>>> =
        repoMain.receiveList(page)

    fun docReceive(docno: String): LiveData<BaseResponse<DocData>> =
        repoMain.docReceive(docno)

}
