package com.jsl.scanner.ui.receive

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.receive.ReceiveListData
import com.jsl.scanner.databinding.ViewReceiveListBinding

class ReceiveListAdapter(
    private val activity: Activity,
    private var data: MutableList<ReceiveListData>,
    val listener: Listener,

    ) : RecyclerView.Adapter<ReceiveListAdapter.Holder>() {
    val res = HashMap<String, Any>()
    fun setItems(data: MutableList<ReceiveListData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<ReceiveListData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReceiveListAdapter.Holder {
        val binding =
            ViewReceiveListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)

    }

    inner class Holder(
        private val binding: ViewReceiveListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(mData: ReceiveListData, position: Int, listener: ReceiveListAdapter.Listener) {
            with(binding) {
                tvDocumentId.text = mData.documentid
                tvJumlahDo.text = mData.doc_count
                itemView.setOnClickListener {
                    val i = Intent(activity, MaterialReceiveActivity::class.java)
                    i.putExtra("docid", mData.documentid)

                    activity.startActivity(i)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ReceiveListAdapter.Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    interface Listener {
        fun onLoadBom(data: ReceiveListData)
    }


}