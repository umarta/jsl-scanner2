package com.jsl.scanner.ui.receive

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.receive.DoListData
import com.jsl.scanner.data.remote.response.receive.PartListData
import com.jsl.scanner.databinding.ActivityMaterialReceiveBinding
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MaterialReceiveActivity : AppCompatActivity(), BaseApp.Listener, PartListAdapter.Listener,
    DoListAdapter.Listener {

    private lateinit var wh: IdNameData
    private lateinit var locator: IdNameData
    private var mediaPlayer: MediaPlayer? = null
    private var mediaPlayerFail: MediaPlayer? = null
    private val viewModel: ScanViewModel by viewModels()
    private lateinit var partListAdapter: PartListAdapter
    private lateinit var doListAdapter: DoListAdapter

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    private var docid: String = ""
    private var isAuto: Boolean = true
    private var type: String = "DOC"
    private var qty: Int = 1

    private lateinit var binding: ActivityMaterialReceiveBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMaterialReceiveBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
        if (intent.hasExtra("docid")) {
            docid = intent.getStringExtra("docid").toString()
        }
    }

    override fun setOnClick() {
        binding.apply {
            ivBack.setOnClickListener { onBackPressed() }
            spWarehouse.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    wh = item
                    getLocator(wh.id, "")
                    viewModel.setWarehouse(wh)
                }
            )
            spLocator.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    locator = item
                    viewModel.setLocator(locator)
                }
            )

            inputProductCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    if (isAuto) {
                        scan()
                    } else {
                        inputQty.requestFocus()
                    }
                }
                false
            })
            inputQty.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    qty = inputQty.text.toString().toInt()
                    scan()
                }
                false
            })
            inputDocumentCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    doc()
                }
                false
            })
        }


    }

    @SuppressLint("SetTextI18n")
    fun doc() {
        var warehouseId = "-";
        var locatorId = "-"

        if (this::wh.isInitialized) {
            warehouseId = wh.id
        } else {
            toast("Pilih warehouse terlebih dahulu")
            binding.progressBar.hide()
            mediaPlayerFail?.start()

            return
        }
        if (this::locator.isInitialized) {
            locatorId = locator.id
        } else {
            toast("Pilih locator terlebih dahulu")
            binding.progressBar.hide()
            mediaPlayerFail?.start()

            return
        }

        val document_code = binding.inputDocumentCode.text.toString()

        val req = HashMap<String, Any>()
        req["m_warehouse"] = wh.name
        req["m_locator"] = locator.name
        req["do_number"] = document_code
        req["qty"] = qty
        if (docid.length > 0) {
            req["documentid"] = docid
        }
        binding.inputDocumentCode.isEnabled = false

        viewModel.docScan(req).observe(this, Observer {
            if (it.success == true) {
                partListAdapter.clear()
                doListAdapter.clear()
                binding.tvTitle.text = "Material Receive - " + it.data.documentid

                partListAdapter.setItems(it.data.list_part)
                doListAdapter.setItems(it.data.list_do)
                binding.rvPartBomList.show()
                binding.rvOpnameList.show()
                binding.nvDocumentList.show()

                viewModel.setDocList(it.data.list_do)
                viewModel.setPartList(it.data.list_part)
                binding.inputDocumentCode.isEnabled = true
                binding.inputDocumentCode.requestFocus()
                binding.inputDocumentCode.text?.clear()
                mediaPlayer?.start()
                docid = it.data.documentid.toString()

            } else {
                binding.inputDocumentCode.isEnabled = true
                binding.inputDocumentCode.text?.clear()
                binding.inputDocumentCode.requestFocus()

                mediaPlayerFail?.start()

            }
        })
    }

    @SuppressLint("SetTextI18n")
    fun scan() {
        binding.apply {
            val product_code = inputProductCode.text.toString()

            val req = HashMap<String, Any>()
            req["product_code"] = product_code
            req["documentid"] = docid
            req["is_auto"] = isAuto
            req["locator"] = locator.name
            req["qty"] = qty
            inputProductCode.isEnabled = false
            if (docid.length < 1) {
                toast("Dokumen kosong, silahkan scan dokumen terlebih dahulu")
                progressBar.hide()
                mediaPlayerFail?.start()
            }
            viewModel.receiveScan(req).observe(this@MaterialReceiveActivity, Observer {
                if (it.success == true) {
                    cardResult.show()
                    tvTitle.text = "Material Receive - " + it.data.documentid

                    tvProductCode.text = it.data.name
                    tvQtyOpname.text = it.data.qtydo
                    tvQtyOnHand.text = it.data.qtyscan
                    tvQtyOnHand.text = it.data.qtyscan
                    tvProductCode.text = it.data.product

                    inputProductCode.text?.clear()
                    inputQty.text?.clear()
                    inputProductCode.isEnabled = true
                    inputProductCode.requestFocus()
                    mediaPlayer?.start()
                } else {
                    inputProductCode.isEnabled = true
                    inputProductCode.text?.clear()

                    mediaPlayerFail?.start()
                }
            })
        }
    }

    override fun setAdapter() {
        partListAdapter = PartListAdapter(this, ArrayList(), this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.rvPartBomList.adapter = partListAdapter
        binding.rvPartBomList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        doListAdapter = DoListAdapter(this, ArrayList(), this)

        binding.rvDocumentList.adapter = doListAdapter
        binding.rvDocumentList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


    }

    override fun setContent() {
        partListAdapter.clear()
        doListAdapter.clear()
        warehouse("")
        mediaPlayer = MediaPlayer.create(this, R.raw.okay)
        mediaPlayerFail = MediaPlayer.create(this, R.raw.no)
        binding.rbScanType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            val rb: RadioButton = findViewById<View>(checkedId) as RadioButton

            if (rb.getText().toString() == "DOC") {
                type = "DOC"
                binding.TILAutoGroup.hide()
                binding.llInputDocument.show()
                binding.llProductCode.hide()
                binding.inputDocumentCode.requestFocus()
            } else {
                type = "scan"
                binding.TILAutoGroup.show()
                binding.llInputDocument.hide()
                binding.llProductCode.show()
                binding.inputProductCode.requestFocus()
            }
            binding.inputProductCode.requestFocus()
        })

        binding.autoGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            val rb: RadioButton = findViewById<View>(checkedId) as RadioButton

            if (rb.getText().toString() == "Tidak") {
                binding.tlInputQty.show()
                isAuto = false

            } else {
                binding.tlInputQty.hide()
                isAuto = true

            }
        })


    }

    override fun loadData() {
        if (docid != "") {
            getDocData()
        }
    }

    @SuppressLint("SetTextI18n")
    fun getDocData() {
        viewModel.docReceive(docid).observe(this, Observer {
            if (it.success!!) {
                binding.tvTitle.text = "Material Receive - " + it.data.documentid
                partListAdapter.setItems(it.data.list_part)
                doListAdapter.setItems(it.data.list_do)
                binding.rvPartBomList.show()
                binding.rvOpnameList.show()
                binding.nvDocumentList.show()

                viewModel.setDocList(it.data.list_do)
                viewModel.setPartList(it.data.list_part)
            }
        })
    }

    private fun warehouse(query: String) {
        viewModel.warehouse(query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spWarehouse.setAdapter(spinnerAdapter)
            }
        })
    }

    private fun getLocator(WarehouseId: String, query: String) {
        viewModel.locator(WarehouseId, query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spLocator.setAdapter(spinnerAdapter)
                binding.rlLocator.show()
            }
        })
    }

    override fun onLoadBom(data: PartListData) {
    }

    override fun onLoadBom(data: DoListData) {
    }
}