package com.jsl.scanner.ui.move

import java.math.BigDecimal
import java.util.*


class DetailData {
    var id = 0
    var invoiceNumber = 0
    var invoiceDate: Date? = null
    var customerName: String? = null
    var customerAddress: String? = null
    var invoiceAmount: BigDecimal? = null
    var amountDue: BigDecimal? = null
}