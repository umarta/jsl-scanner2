package com.jsl.scanner.ui.receive

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.receive.PartListData
import com.jsl.scanner.databinding.ViewPartListBinding

class PartListAdapter(
    private val activity: Activity,
    private var data: MutableList<PartListData>,
    val listener: PartListAdapter.Listener,
) : RecyclerView.Adapter<PartListAdapter.Holder>() {
    val res = HashMap<String, Any>()

    fun setItems(data: MutableList<PartListData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<PartListData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding =
            ViewPartListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(
        private val binding: ViewPartListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(mData: PartListData, position: Int, listener: Listener) {
            with(binding) {
                tvKodePart.setText("Kode Part :" + mData.product)
                tvNamaPart.setText("Nama Part :" + mData.name)
                tvQty.setText("Qty :" + mData.qtydo.toString())
                tvQtyScan.setText("Qty Scan :" + mData.qtyscan.toString())
            }
        }
    }

    interface Listener {
        fun onLoadBom(data: PartListData)
    }

}