package com.jsl.scanner.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.jsl.scanner.R
import com.jsl.scanner.databinding.DialogCloseBinding

class CloseDialog(val message: String, val listener: Listener) : DialogFragment() {

    private lateinit var binding: DialogCloseBinding

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(R.color.transparent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        binding = DialogCloseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvText.text = message

        binding.tvCancel.setOnClickListener { dismiss() }
        binding.tvAction.setOnClickListener {
            listener.onCloseDialog()
            dismiss()
        }
    }

    interface Listener {
        fun onCloseDialog()
    }

}