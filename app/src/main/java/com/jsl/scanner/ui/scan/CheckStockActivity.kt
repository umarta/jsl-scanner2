package com.jsl.scanner.ui.scan

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.scan.CheckResultData
import com.jsl.scanner.databinding.ActivityCheckStockBinding
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckStockActivity : AppCompatActivity(), BaseApp.Listener, OpnameAdapter.Listener {

    private val viewModel: ScanViewModel by viewModels()
    private lateinit var wh: IdNameData
    private lateinit var opnameAdapter: OpnameAdapter
    private var mediaPlayer: MediaPlayer? = null
    private var mediaPlayerFail: MediaPlayer? = null

    private lateinit var binding: ActivityCheckStockBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckStockBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {

    }

    override fun setOnClick() {
        binding.apply {
            ivBack.setOnClickListener { onBackPressed() }
            inputProductCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    fetchData()
                }
                false
            })
            spWarehouse.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    wh = item
                    viewModel.setWarehouse(wh)

                }
            )
        }
    }

    //gninbrmgot
    @SuppressLint("SetTextI18n")
    fun fetchData() {
        var warehouseId = "-";

        if (this::wh.isInitialized) {
            warehouseId = wh.id
        } else {
            toast("Pilih warehouse terlebih dahulu")
            binding.progressBar.hide()
            mediaPlayerFail?.start()
            return
        }
        viewModel.checkStock(wh.id, binding.inputProductCode.text.toString())
            .observe(this, Observer {
                if (it.success!!) {
                    opnameAdapter.setItems(it.data)
                    binding.llCheck.show()
                    binding.rvOpnameList.show()
                    if (it.data.size > 0) {
                        mediaPlayer?.start()
                        binding.tvKodePart.text = "Kode : " + it.data[0].kodeproduk
                        binding.tvNamaPart.text = "Nama : " + it.data[0].namaproduk
                        binding.tvSku.text = "SKU : " + it.data[0].sku
//                        tv_kode_part.textSize = 24F
//                        tv_nama_part.textSize = 24F
//                        tv_sku.textSize = 24F
                    } else {
                        mediaPlayerFail?.start()

                    }
                    binding.inputProductCode.text?.clear()


                } else {
                    mediaPlayerFail?.start()

                }
            })
    }

    override fun setAdapter() {
    }

    override fun setContent() {
        opnameAdapter = OpnameAdapter(this, ArrayList(), this, "check_stock")
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.rvPartBomList.adapter = opnameAdapter
        binding.rvPartBomList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        warehouse("")
        mediaPlayer = MediaPlayer.create(this, R.raw.okay)
        mediaPlayerFail = MediaPlayer.create(this, R.raw.no)

    }

    override fun loadData() {
    }

    override fun onLoadBom(data: CheckResultData) {
    }

    private fun warehouse(query: String) {
        viewModel.warehouse(query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spWarehouse.setAdapter(spinnerAdapter)
            }
        })
    }
}