package com.jsl.scanner.ui.assembling.active

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsl.scanner.R
import com.jsl.scanner.data.move.MoveManager
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityListBinding
import com.jsl.scanner.ui.assembling.detail.AssemblingDetailActivity
import com.jsl.scanner.ui.dialog.AddNewDataDialog
import com.jsl.scanner.ui.move.InventoryMoveViewModel
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.Constants
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint

class ListActivity : AppCompatActivity(), BaseApp.Listener, ListAdapter.Listener,
    WarehouseSheet.Listener {
    @Inject
    lateinit var userManager: UserManager

    @Inject
    lateinit var moveManager: MoveManager
    private lateinit var listAdapter: ListAdapter
    private lateinit var addNewDataDialog: AddNewDataDialog
    private val viewModel: InventoryMoveViewModel by viewModels()
    private val viewModelScan: ScanViewModel by viewModels()
    private var warehouseData: IdNameData? = null
    private var locatorList: MutableList<IdNameData>? = null
    private var locatorData: IdNameData? = null
    private lateinit var wh: IdNameData

    private var warehouseList: MutableList<IdNameData> = ArrayList()
    private lateinit var warehouseSheet: WarehouseSheet

    private lateinit var binding: ActivityListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
//        iv_close.setOnClickListener {
//        }

        binding.ivAdd.setOnClickListener {
//            val i = Intent(this, AssemblingDetailActivity::class.java)
//            i.putExtra(Constants.TRANSACTION_ID, "0")
//            this.startActivity(i)
            warehouseSheet =
                WarehouseSheet(this, warehouseList, locatorList, warehouseData, locatorData)
            warehouseSheet.show(supportFragmentManager, WarehouseSheet::class.java.name)
            warehouse("")

        }
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

    }

    override fun setAdapter() {
        listAdapter = ListAdapter("Pengeluaran", this, ArrayList(), this)
        binding.rvActiveTransaction.adapter = listAdapter
        binding.rvActiveTransaction.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })

    }

    private fun warehouse(query: String) {
        viewModelScan.warehouse(query).observe(this, Observer {
            if (it.success!!) {
                warehouseSheet.setWarehouse(it.data)
            }
        })
    }

    override fun loadData() {
        viewModel.activeList(userManager.warehouseId, "Assembly").observe(this, Observer {
            if (it.success!!) {
                listAdapter.setItems(it.data)
            }
        })
    }


    override fun onActiveTrx(data: IdNameData) {
        finish()
    }

    override fun onWarehouse(warehouse: IdNameData) {
        moveManager.setTargetWHId(warehouse.id, warehouse.name)
    }


    override fun onAdd() {
        viewModel.addNewTrx(
            userManager.warehouseId,
            moveManager.getTargetWHId,
            moveManager.getOriginLocatorId
        )
            .observe(this, Observer {
                if (it.success!!) {
                    val i = Intent(this, AssemblingDetailActivity::class.java)
                    i.putExtra(Constants.TRANSACTION_ID, it.data.id)
                    toast(it.data.id)
                    this.startActivity(i)
                } else {
                    toast("Gagal menambahkan transaksi")
                }
            })
        toast(userManager.warehouseId + " - " + moveManager.getTargetWHId)
    }

}