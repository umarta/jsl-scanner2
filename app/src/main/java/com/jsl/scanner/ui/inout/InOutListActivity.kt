package com.jsl.scanner.ui.inout

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.move.InOutData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityInOutListBinding
import com.jsl.scanner.ui.receive.MaterialReceiveActivity
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InOutListActivity : AppCompatActivity(), BaseApp.Listener, InOutListAdapter.Listener,
    CreateHeaderDialog.Listener {

    @Inject
    lateinit var userManager: UserManager
    private val viewModel: ScanViewModel by viewModels()
    private lateinit var inOutListAdapter: InOutListAdapter
    private var next_page = true
    private var loading = false
    private var page = 1

    private var wh_from: String? = null
    private var wh_to: String? = null
    var whList: MutableList<IdNameData> = ArrayList()

    private lateinit var binding: ActivityInOutListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInOutListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun setOnClick() {
        binding.apply {
            ivBack.setOnClickListener { onBackPressed() }
            swipeContainer.setOnRefreshListener {
                inOutListAdapter.clear()
                page = 1
                fetchData()
                swipeContainer.setRefreshing(false)
            }
            swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
            )
            ivAdd.setOnClickListener {
                val i = Intent(this@InOutListActivity, MaterialReceiveActivity::class.java)
                this@InOutListActivity.startActivity(i)
            }
            ivAdd.setOnClickListener {
                val fm: FragmentManager = supportFragmentManager
                val dialog =
                    CreateHeaderDialog(
                        "Pilih bulan dan tahun untuk generate shift?",
                        this@InOutListActivity,
                        whList
                    )

                dialog.show(fm, CreateHeaderDialog::javaClass.name)
            }
        }
    }

    fun fetchData() {
        viewModel.inOutList(page).observe(this, Observer {
            if (it.success!!) {
                next_page = it.header.next_page
                if (page == 1) {
                    inOutListAdapter.setItems(it.data)
                } else {
                    inOutListAdapter.addItems(it.data)
                }
            }
        })
    }

    fun fetchWarehouse() {
//        viewModel.warehouse(userManager.warehouseName).observe(this, Observer {
//            if (it.success!!) {
//                whList = it.data
//            }
//        })
        whList.add(IdNameData(userManager.warehouseId, userManager.warehouseName))
    }

    fun createHeader() {
        val req = HashMap<String, Any>()
        req["wh_from"] = wh_from.toString()
        req["wh_to"] = wh_to.toString()

        viewModel.createHeaderMove(req).observe(this, Observer {
            if (it.success!!) {
                toast("success")
                val i = Intent(this, InOutActivity::class.java)
                i.putExtra("move_id", it.data.documentno)
                i.putExtra("wh_from", it.data.from_wh)
                i.putExtra("wh_to", it.data.to_wh)

                this.startActivity(i)

            }
        })
    }


    override fun setAdapter() {
        inOutListAdapter = InOutListAdapter(this, ArrayList(), this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.rvSerahterima.adapter = inOutListAdapter
        binding.rvSerahterima.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvSerahterima.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (!recyclerView.canScrollVertically(1)) {
                    if (next_page) {
                        if (!loading && isLastPosition) {
                            page++
                            loadData()
                        }
                    }
                }
            }

        })

    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
    }

    override fun loadData() {
        fetchData()
        fetchWarehouse()
    }

    override fun onActiveTrx(data: InOutData) {
        finish()
    }

    override fun onCloseDialog(wh_from_: String, wh_to_: String) {
        wh_from = wh_from_
        wh_to = wh_to_
        createHeader()

    }

    override fun cancelClose() {
    }
}