package com.jsl.scanner.ui.receive

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.receive.ReceiveListData
import com.jsl.scanner.databinding.ActivityMaterialReceiveListBinding
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MaterialReceiveListActivity : AppCompatActivity(), BaseApp.Listener,
    ReceiveListAdapter.Listener {

    private val viewModel: ScanViewModel by viewModels()
    private lateinit var receiveListAdapter: ReceiveListAdapter
    private var next_page = true
    private var loading = false
    private var page = 1

    private lateinit var binding: ActivityMaterialReceiveListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMaterialReceiveListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        binding.ivBack.setOnClickListener { onBackPressed() }
        binding.swipeContainer.setOnRefreshListener {
            receiveListAdapter.clear()
            page = 1
            fetchData()
            binding.swipeContainer.setRefreshing(false)
        }

        binding.swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );

        binding.ivAdd.setOnClickListener {
            val i = Intent(this, MaterialReceiveActivity::class.java)

            this.startActivity(i)

        }
    }

    override fun setAdapter() {
        receiveListAdapter = ReceiveListAdapter(this, ArrayList(), this)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.rvSerahterima.adapter = receiveListAdapter
        binding.rvSerahterima.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvSerahterima.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (!recyclerView.canScrollVertically(1)) {
                    if (next_page) {
                        if (!loading && isLastPosition) {
                            page++
                            loadData()
                        }
                    }
                }
            }

        })
    }

    fun fetchData() {
        viewModel.receiveList(page).observe(this, Observer {
            if (it.success!!) {
                next_page = it.header.next_page
                if (page == 1) {
                    receiveListAdapter.setItems(it.data)
                } else {
                    receiveListAdapter.addItems(it.data)
                }
            }
        })
    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })

    }

    override fun loadData() {
        fetchData()

    }

    override fun onLoadBom(data: ReceiveListData) {
    }
}