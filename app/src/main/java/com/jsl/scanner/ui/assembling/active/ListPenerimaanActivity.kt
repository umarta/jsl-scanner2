package com.jsl.scanner.ui.assembling.active

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsl.scanner.data.move.MoveManager
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityListPenerimaanBinding
import com.jsl.scanner.ui.move.InventoryMoveViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint

class ListPenerimaanActivity : AppCompatActivity(), BaseApp.Listener, ListAdapter.Listener {
    @Inject
    lateinit var userManager: UserManager

    @Inject
    lateinit var moveManager: MoveManager
    private lateinit var listAdapter: ListAdapter

    private val viewModel: InventoryMoveViewModel by viewModels()

    private lateinit var binding: ActivityListPenerimaanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListPenerimaanBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun setAdapter() {
        listAdapter = ListAdapter("Penerimaan", this, ArrayList(), this)
        binding.rvActiveTransaction.adapter = listAdapter
        binding.rvActiveTransaction.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    }

    override fun setContent() {

        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
    }

    override fun loadData() {
        viewModel.activeListPenerimaanAss(userManager.warehouseId, "Assembly")
            .observe(this, Observer {
                if (it.success!!) {
                    listAdapter.setItems(it.data)
                }
            })
    }

    override fun onActiveTrx(data: IdNameData) {
    }
}