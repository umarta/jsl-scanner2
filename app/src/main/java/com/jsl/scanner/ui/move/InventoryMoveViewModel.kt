package com.jsl.scanner.ui.move

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jsl.scanner.data.remote.MainRepository
import com.jsl.scanner.data.remote.MainService
import com.jsl.scanner.data.remote.response.BaseResponse
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.move.AddNewTrx
import com.jsl.scanner.data.remote.response.move.ChangeLocator
import com.jsl.scanner.data.remote.response.move.FetchResultData
import com.jsl.scanner.data.remote.response.move.MoveScanData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InventoryMoveViewModel @Inject constructor(mainService: MainService) : ViewModel() {
    private val repoMain =
        MainRepository(mainService)
    val loading: LiveData<Boolean> get() = repoMain.loading

    fun fetchDoc(
        query: String,
        transactionId: String,
        warehouseId: String,
        moveType: String
    ): MutableLiveData<BaseResponse<FetchResultData>> =
        repoMain.fetchDoc(query, transactionId, warehouseId, moveType)

    fun scanProduct(
        productNo: String,
        qty: String,
        isScan: Boolean,
        transactionId: String,
        type: String,
        locator: String
    ): MutableLiveData<BaseResponse<MutableList<MoveScanData>>> =
        repoMain.scanProduct(productNo, qty, isScan, transactionId, type, locator)

    fun scanPenerimaan(
        productNo: String,
        qty: String,
        isScan: Boolean,
        transactionId: String,
        type: String,
        locator: String
    ): MutableLiveData<BaseResponse<MutableList<MoveScanData>>> =
        repoMain.scanPenerimaan(productNo, qty, isScan, transactionId, type, locator)

    fun changeLocator(
        locator: String,
        scanId: String
    ): MutableLiveData<BaseResponse<ChangeLocator>> =
        repoMain.changeLocator(locator, scanId)

    fun activeList(
        warehouseId: String,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.activeList(warehouseId, type)

    fun activeListPenerimaanAss(
        warehouseId: String,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<IdNameData>>> =
        repoMain.activeListPenerimaanAss(warehouseId, type)

    fun selectedTrx(
        transactionId: String,
        type: String
    ): MutableLiveData<BaseResponse<FetchResultData>> =
        repoMain.selectedTrx(transactionId, type)

    fun detailPenerimaanAss(
        transactionId: String,
        type: String
    ): MutableLiveData<BaseResponse<MutableList<MoveScanData>>> =
        repoMain.detailPenerimaanAss(transactionId, type)


    fun confirmTrx(transactionId: String, type: String): MutableLiveData<BaseResponse<Any>> =
        repoMain.confirmTrx(transactionId, type)

    fun closeReceivingASS(transactionId: String): MutableLiveData<BaseResponse<Any>> =
        repoMain.closeReceivingASS(transactionId)

    fun addNewTrx(
        warehouseFromId: String,
        warehouseToId: String,
        originLocator: String
    ): MutableLiveData<BaseResponse<AddNewTrx>> =
        repoMain.addNewTrx(warehouseFromId, warehouseToId, originLocator)

}