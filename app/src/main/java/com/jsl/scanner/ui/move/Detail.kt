package com.jsl.scanner.ui.move

import java.math.BigDecimal
import java.util.*

class Detail {
    val Detail: Array<DetailData?>
        get() {
            val data: Array<DetailData?> = arrayOfNulls<DetailData>(20)
            for (i in 0..19) {
                val row = DetailData()
                row.id = i + 1
                row.invoiceNumber = row.id
                row.amountDue = BigDecimal.valueOf(20.00 * i)
                row.invoiceAmount = BigDecimal.valueOf(120.00 * (i + 1))
                row.invoiceDate = Date()
                row.customerName = "Thomas John Beckett"
                row.customerAddress = "1112, Hash Avenue, NYC"
                data[i] = row
            }
            return data
        }
}