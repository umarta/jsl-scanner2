package com.jsl.scanner.ui.assembling.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.move.MoveManager
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.remote.response.move.FetchResultData
import com.jsl.scanner.data.remote.response.move.MoveScanData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityAsemmblingDetailBinding
import com.jsl.scanner.ui.assembling.active.ListActivity
import com.jsl.scanner.ui.assembling.active.WarehouseSheet
import com.jsl.scanner.ui.dialog.CloseDialog
import com.jsl.scanner.ui.move.InventoryMoveViewModel
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.Constants
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import com.jsl.scanner.vo.HttpCode
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AssemblingDetailActivity : AppCompatActivity(), BaseApp.Listener, WarehouseSheet.Listener,
    CloseDialog.Listener {
    @Inject
    lateinit var moveManager: MoveManager

    @Inject
    lateinit var userManager: UserManager
    private var TRANSACTION_ID: String? = null
    private val viewModelScan: ScanViewModel by viewModels()

    private val viewModel: InventoryMoveViewModel by viewModels()
    private var mediaPlayer: MediaPlayer? = null
    private var mediaPlayerFail: MediaPlayer? = null
    private var warehouseData: IdNameData? = null
    private var locatorList: MutableList<IdNameData>? = null
    private var locatorData: IdNameData? = null
    private val scanViewModel: ScanViewModel by viewModels()

    private var warehouseList: MutableList<IdNameData> = ArrayList()
    private lateinit var warehouseSheet: WarehouseSheet

    private lateinit var binding: ActivityAsemmblingDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAsemmblingDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()

    }

    private fun locator(WarehouseId: String, query: String) {
        scanViewModel.locator(WarehouseId, query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spLocator.setAdapter(spinnerAdapter)
                binding.rlLocator.show()
            }
        })
    }

    override fun getIntentData() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
        if (intent.hasExtra(Constants.TRANSACTION_ID)) {
            TRANSACTION_ID = intent.getStringExtra(Constants.TRANSACTION_ID)
        } else {
            toast(getString(R.string.data_empty))
            finish()
        }

    }

    private fun showCloseDialog() {
        val fm: FragmentManager = supportFragmentManager
        val dialog = CloseDialog("Yakin untuk menutup transaksi ini ?", this)
        dialog.show(fm, CloseDialog::javaClass.name)
    }

    override fun setOnClick() {
        binding.apply {
            ivAdd.setOnClickListener {
                warehouseSheet =
                    WarehouseSheet(
                        this@AssemblingDetailActivity,
                        warehouseList,
                        locatorList,
                        warehouseData,
                        locatorData
                    )
                warehouseSheet.show(supportFragmentManager, WarehouseSheet::class.java.name)
                warehouse("")
            }
            ivBack.setOnClickListener { onBackPressed() }
            ivClose.setOnClickListener {
                showCloseDialog()
            }
            moveAutoGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
                val rb: RadioButton = findViewById<View>(checkedId) as RadioButton
                if (rb.text.toString() == "Tidak") {
                    tlInputQty.show()
                    tvSave.show()
                    inputQty.text?.clear()
                } else {
                    tlInputQty.hide()
                    inputQty.setText("1")
                    tvSave.hide()
                }
            })
            rbScanType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
                val rb: RadioButton = findViewById<View>(checkedId) as RadioButton
                if (rb.text.toString() == "Edit") {
                    tlInputQty.show()
                    tvSave.show()
                    inputQty.text?.clear()

                } else {
                    tlInputQty.hide()
                    inputQty.setText("1")
                    tvSave.hide()
                }
            })

            spLocator.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    locatorData = item
                }
            )

            inputProductCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    val isScan =
                        rbScanType.checkedRadioButtonId // R.id.male, int value 2131361895
                    val isScanString =
                        resources.getResourceEntryName(isScan)
                            .toString() == "rb_check_act"   // "male"
                    if (!isScanString) {
                        scanProduct()
                    }
                }
                false
            })
        }
    }

    override fun setAdapter() {
    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
    }

    private fun warehouse(query: String) {
        viewModelScan.warehouse(query).observe(this, Observer {
            if (it.success!!) {
                warehouseSheet.setWarehouse(it.data)
            }
        })
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun generateTable(detail: ArrayList<FetchResultData.Detail?>) {
        val ll =
            findViewById<View>(R.id.table_list) as TableLayout
        ll.removeViews(1, 0.coerceAtLeast(ll.childCount - 1))
        var row = TableRow(this)
        row.setBackgroundResource(R.color.green)
        row.setPadding(10)
        var lp: TableRow.LayoutParams? =
            TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        var numeric: TableRow.LayoutParams? =
            TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)

        row.layoutParams = lp
        lp?.weight = 1F
        var no = TextView(this)
        var kode = TextView(this)
        var sku = TextView(this)
        var qty_scan = TextView(this)
        var name = TextView(this)
        for ((i, item) in detail.withIndex()) {
            row = TableRow(this)
//            row.setPadding(10)
            lp = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
            row.layoutParams = lp
//            row.background = getDrawable(R.drawable.border)
            lp.weight = 1F

            no = TextView(this)
            kode = TextView(this)
            sku = TextView(this)
            qty_scan = TextView(this)
            name = TextView(this)

            no.background = getDrawable(R.drawable.border)
            kode.background = getDrawable(R.drawable.border)
            sku.background = getDrawable(R.drawable.border)
            qty_scan.background = getDrawable(R.drawable.border)
            name.background = getDrawable(R.drawable.border)

            no.setPadding(10)
            kode.setPadding(10)
            sku.setPadding(10)
            qty_scan.setPadding(10)
            name.setPadding(10)


            no.layoutParams = lp
            kode.layoutParams = lp
            sku.layoutParams = lp
            qty_scan.layoutParams = lp
            name.layoutParams = lp

            no.text = (i + 1).toString()
            sku.text = item?.sku
            qty_scan.text = item?.qty_idem
            kode.text = item?.kode_produk

            name.text = item?.product_name


            row.addView(no)
            row.addView(sku)
            row.addView(qty_scan)
            row.addView(kode)

            row.addView(name)
            ll.addView(row, i + 1)
        }
    }

    override fun loadData() {
        if (TRANSACTION_ID != "0")
            viewModel.selectedTrx(TRANSACTION_ID!!, "Assembly").observe(this, Observer {
                if (it.code == HttpCode.SUCCESS) {
                    binding.inputProductCode.requestFocus()
                    generateTable(it.data.detail)
                    toast(it.message?.get(0))
                } else {
                    toast(it.message?.get(0))
                }
            })
        locator(moveManager.getTargetWHId.toString(), "")
    }

    private fun generateTableScan(detail: MutableList<MoveScanData>) {
        val ll =
            findViewById<View>(R.id.table_list) as TableLayout
        ll.removeViews(1, 0.coerceAtLeast(ll.childCount - 1))
        var row = TableRow(this)
        row.setBackgroundResource(R.color.green)
        row.setPadding(10)
        var lp: TableRow.LayoutParams? =
            TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        row.layoutParams = lp
        lp?.weight = 1F
        var no = TextView(this)
        var kode = TextView(this)
        var sku = TextView(this)
        var qty_scan = TextView(this)
        var name = TextView(this)
        for ((i, item) in detail.withIndex()) {
            row = TableRow(this)
            row.setPadding(10)
            lp = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
            row.layoutParams = lp
            lp.weight = 1F

            no = TextView(this)
            sku = TextView(this)
            qty_scan = TextView(this)
            kode = TextView(this)

            name = TextView(this)

            no.layoutParams = lp
            sku.layoutParams = lp
            qty_scan.layoutParams = lp
            kode.layoutParams = lp

            name.layoutParams = lp


            no.text = (i + 1).toString()
            sku.text = item.sku
            qty_scan.text = item.qty_idem
            kode.text = item.kode_produk
            name.text = item.product_name

            row.addView(no)
            row.addView(sku)
            row.addView(qty_scan)
            row.addView(kode)
            row.addView(name)
            ll.addView(row, i + 1)
        }
    }

    private fun scanProduct() {
        binding.apply {
            progressBar.show()

            val productCode = inputProductCode.text.toString()
            val qty = inputQty.text.toString()
            val isScan = rbScanType.checkedRadioButtonId // R.id.male, int value 2131361895
            val isScanString =
                resources.getResourceEntryName(isScan).toString() == "rb_check_act"   // "male"
            val locatorId = locatorData!!.id
            viewModel.scanProduct(
                productCode,
                qty.ifEmpty { "1" },
                isScanString,
                TRANSACTION_ID.toString(),
                "Assembly",
                locatorId
            ).observe(this@AssemblingDetailActivity, Observer {
                if (it.code == HttpCode.SUCCESS) {
                    if (it.data.size > 0) {
                        generateTableScan(it.data)
                        inputProductCode.text?.clear()
                    }
                    progressBar.hide()
                    mediaPlayer?.start()

                    toast(it.message?.get(0))

                } else {
                    progressBar.hide()
                    mediaPlayerFail?.start()

                    toast(it.message?.get(0))
                }
            })
        }
    }

    override fun onWarehouse(warehouse: IdNameData) {
        moveManager.setTargetWHId(warehouse.id, warehouse.name)
    }

    override fun onAdd() {
        viewModel.addNewTrx(
            userManager.warehouseId,
            moveManager.getTargetWHId,
            moveManager.getOriginLocatorId
        )
            .observe(this, Observer {
                if (it.success!!) {
                    val i = Intent(this, AssemblingDetailActivity::class.java)
                    i.putExtra(Constants.TRANSACTION_ID, it.data.transaksi_id)
                    this.startActivity(i)
                } else {
                    toast("Gagal menambahkan transaksi")
                }
            })
        toast(userManager.warehouseId + " - " + moveManager.getTargetWHId)
    }

    override fun onCloseDialog() {
        viewModel.confirmTrx(TRANSACTION_ID!!, "Assembly").observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                startActivity(Intent(this, ListActivity::class.java))
//                this.finishAffinity()
                toast(it.message?.get(0))
            } else {
                toast(it.message?.get(0))
            }
        })
    }
}