package com.jsl.scanner.ui.inout

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jsl.scanner.data.remote.response.move.InOutData
import com.jsl.scanner.databinding.ViewInoutListBinding

class InOutListAdapter(
    private val activity: Activity,
    private var data: MutableList<InOutData>,
    val listener: InOutListAdapter.Listener,
) : RecyclerView.Adapter<InOutListAdapter.Holder>() {
    val res = HashMap<String, Any>()

    fun setItems(data: MutableList<InOutData>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun addItems(data: MutableList<InOutData>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding =
            ViewInoutListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)

    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position], position, listener)
    }

    override fun getItemCount(): Int = data.size

    inner class Holder(
        private val binding: ViewInoutListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(mData: InOutData, position: Int, listener: InOutListAdapter.Listener) {
            with(binding) {
                tvDocumentNo.text = "No : " + mData.documentno
                tvDocumentDate.text = "Date : " + mData.document_date
                tvFrom.text = "From : " + mData.from_wh
                tvTo.text = "To : " + mData.to_wh

                itemView.setOnClickListener {
                    val i = Intent(activity, InOutActivity::class.java)
                    i.putExtra("move_id", mData.documentno)
                    i.putExtra("wh_from", mData.from_wh)
                    i.putExtra("wh_to", mData.to_wh)
                    activity.startActivity(i)
                }
            }
        }
    }

    interface Listener {
        fun onActiveTrx(data: InOutData)
    }

}