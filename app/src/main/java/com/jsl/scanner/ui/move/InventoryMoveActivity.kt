package com.jsl.scanner.ui.move

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.jsl.scanner.R
import com.jsl.scanner.data.move.MoveManager
import com.jsl.scanner.data.remote.response.move.FetchResultData
import com.jsl.scanner.data.remote.response.move.MoveScanData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityInventoryMoveBinding
import com.jsl.scanner.ui.dialog.CloseDialog
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.Constants
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import com.jsl.scanner.vo.HttpCode
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class InventoryMoveActivity : AppCompatActivity(), BaseApp.Listener, CloseDialog.Listener {

    @Inject
    lateinit var moveManager: MoveManager

    @Inject
    lateinit var userManager: UserManager
    private var TRANSACTION_ID: String? = null
    private lateinit var moveType: String

    private val viewModel: InventoryMoveViewModel by viewModels()
    val loading2 = MutableLiveData<Boolean>()

    private var mediaPlayer: MediaPlayer? = null
    private var mediaPlayerFail: MediaPlayer? = null

    private lateinit var binding: ActivityInventoryMoveBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInventoryMoveBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mediaPlayer = MediaPlayer.create(this, R.raw.okay)
        mediaPlayerFail = MediaPlayer.create(this, R.raw.no)
        BaseApp(this).set()
    }

    private fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager =
            activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            activity.currentFocus!!.windowToken, 0
        )
    }

    private fun fetchDoc() {
        val doc_no = binding.inputDocNo.text.toString()
        viewModel.fetchDoc(
            doc_no, TRANSACTION_ID.toString(), userManager.warehouseId.toString(), moveType
        ).observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                moveManager.moveInventSaveData(it.data)
                TRANSACTION_ID = it.data.header?.transaksi_id.toString()
                if (it.data.detail.size > 0) {
                    binding.inputLocator.setText(it.data.detail.get(0)?.kode_locator_to)
                    generateTable(it.data.detail)
                    toast(it.message?.get(0))
                    mediaPlayer?.start()
                }
            } else {
                mediaPlayerFail?.start()
                toast(it.message?.get(0))
            }
        })
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun generateTable(detail: ArrayList<FetchResultData.Detail?>) {
        val ll =
            findViewById<View>(R.id.table_list) as TableLayout
        ll.removeViews(1, 0.coerceAtLeast(ll.childCount - 1))
        var row = TableRow(this)
        row.setBackgroundResource(R.color.green);
        row.setPadding(10);
        var lp: TableRow.LayoutParams? =
            TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        var numeric: TableRow.LayoutParams? =
            TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)

        row.layoutParams = lp
        lp?.weight = 1F
        var no = TextView(this)
        var kode = TextView(this)
        var sku = TextView(this)
        var qty_idem = TextView(this)
        var qty_scan = TextView(this)
        var name = TextView(this)
        for ((i, item) in detail.withIndex()) {
            row = TableRow(this)
//            row.setPadding(10)
            lp = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
            row.layoutParams = lp
//            row.background = getDrawable(R.drawable.border)
            lp.weight = 1F

            no = TextView(this)
            kode = TextView(this)
            sku = TextView(this)
            qty_idem = TextView(this)
            qty_scan = TextView(this)
            name = TextView(this)

            no.background = getDrawable(R.drawable.border)
            kode.background = getDrawable(R.drawable.border)
            sku.background = getDrawable(R.drawable.border)
            qty_idem.background = getDrawable(R.drawable.border)
            qty_scan.background = getDrawable(R.drawable.border)
            name.background = getDrawable(R.drawable.border)

            no.setPadding(10)
            kode.setPadding(10)
            sku.setPadding(10)
            qty_idem.setPadding(10)
            qty_scan.setPadding(10)
            name.setPadding(10)


            no.layoutParams = lp
            kode.layoutParams = lp
            sku.layoutParams = lp
            qty_idem.layoutParams = lp
            qty_scan.layoutParams = lp
            name.layoutParams = lp

            no.text = (i + 1).toString()
            sku.text = item?.sku
            qty_idem.text = item?.qty_idem
            qty_scan.text = item?.qty_scan
            kode.text = item?.kode_produk

            name.text = item?.product_name


            row.addView(no)
            row.addView(sku)
            row.addView(qty_idem)
            row.addView(qty_scan)
            row.addView(kode)

            row.addView(name)
            ll.addView(row, i + 1)
        }
    }

    private fun generateTableScan(detail: MutableList<MoveScanData>) {
        val ll =
            findViewById<View>(R.id.table_list) as TableLayout
        ll.removeViews(1, 0.coerceAtLeast(ll.childCount - 1))
        var row = TableRow(this)
        row.setBackgroundResource(R.color.green);
        row.setPadding(10);
        var lp: TableRow.LayoutParams? =
            TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        row.layoutParams = lp
        lp?.weight = 1F
        var no = TextView(this)
        var kode = TextView(this)
        var sku = TextView(this)
        var qty_idem = TextView(this)
        var qty_scan = TextView(this)
        var name = TextView(this)
        for ((i, item) in detail.withIndex()) {
            row = TableRow(this)
            row.setPadding(10)
            lp = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
            row.layoutParams = lp
            lp.weight = 1F

            no = TextView(this)
            sku = TextView(this)
            qty_idem = TextView(this)
            qty_scan = TextView(this)
            kode = TextView(this)

            name = TextView(this)

            no.layoutParams = lp
            sku.layoutParams = lp
            qty_idem.layoutParams = lp
            qty_scan.layoutParams = lp
            kode.layoutParams = lp

            name.layoutParams = lp


            no.text = (i + 1).toString()
            sku.text = item.sku
            qty_idem.text = item.qty_idem
            qty_scan.text = item.qty_scan
            kode.text = item.kode_produk
            name.text = item.product_name


            row.addView(no)
            row.addView(sku)
            row.addView(qty_idem)
            row.addView(qty_scan)
            row.addView(kode)
            row.addView(name)
            ll.addView(row, i + 1)
        }
    }


    private fun scanProduct() {
        binding.progressBar.show()
        val productCode = binding.inputProductCode.text.toString()
        val qty = binding.inputQty.text.toString()
        val isScan = binding.rbScanType.checkedRadioButtonId // R.id.male, int value 2131361895
        val isScanString =
            resources.getResourceEntryName(isScan).toString() == "rb_check_act"   // "male"
        viewModel.scanProduct(
            productCode,
            qty.ifEmpty { "1" },
            isScanString,
            TRANSACTION_ID.toString(),
            moveType,
            ""
        ).observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                if (it.data.size > 0) {
                    generateTableScan(it.data)
                    binding.inputProductCode.text?.clear()
                }
                binding.progressBar.hide()
                mediaPlayer?.start()
                toast(it.message?.get(0))
            } else {
                binding.progressBar.hide()
                mediaPlayerFail?.start()

                toast(it.message?.get(0))
            }
        })
    }

    private fun changeLocator() {
        val locator = binding.inputLocator.text.toString()
        binding.progressBar.show()
        viewModel.changeLocator(locator, moveManager.scanId).observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                moveManager.setLocator(it.data)
                binding.inputProductCode.requestFocus()
                toast(it.message?.get(0))
                binding.progressBar.hide()
            } else {
                toast(it.message?.get(0))
                binding.progressBar.hide()
            }
        })
    }

    override fun getIntentData() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
        if (intent.hasExtra(Constants.TRANSACTION_ID)) {
            TRANSACTION_ID = intent.getStringExtra(Constants.TRANSACTION_ID)
        } else {
            toast(getString(R.string.data_empty))
            finish()
        }
        if (intent.hasExtra(Constants.MOVE_TYPE)) {
            moveType = intent.getStringExtra(Constants.MOVE_TYPE).toString()

            binding.tvTitle.text = moveType
        }

    }

    override fun setOnClick() {
        binding.apply {
            ivAdd.setOnClickListener {
                val i = Intent(this@InventoryMoveActivity, InventoryMoveActivity::class.java)
                i.putExtra(Constants.TRANSACTION_ID, "0")
                i.putExtra(Constants.MOVE_TYPE, moveType)
                this@InventoryMoveActivity.startActivity(i)
            }
            ivBack.setOnClickListener { onBackPressed() }

            moveAutoGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
                val rb: RadioButton = findViewById<View>(checkedId) as RadioButton
                if (rb.getText().toString() == "Tidak") {
                    tlInputQty.show()
                    tvSave.show()
                    inputQty.text?.clear()
                } else {
                    tlInputQty.hide()
                    inputQty.setText("1")
                    tvSave.hide()
                }
            })
            rbScanType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
                val rb: RadioButton = findViewById<View>(checkedId) as RadioButton
                if (rb.getText().toString() == "Edit") {
                    tlInputQty.show()
                    tvSave.show()
                    inputQty.text?.clear()
                } else {
                    tlInputQty.hide()
                    inputQty.setText("1")
                    tvSave.hide()
                }
            })


            tvSave.setOnClickListener {
                scanProduct()
            }

            inputDocNo.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    fetchDoc()
                }
                false
            })
            inputProductCode.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    val isScan =
                        rbScanType.checkedRadioButtonId // R.id.male, int value 2131361895
                    val isScanString =
                        resources.getResourceEntryName(isScan)
                            .toString() == "rb_check_act"   // "male"
                    if (!isScanString) {
                        scanProduct()
                    }
                }
                false
            })
            inputLocator.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    changeLocator()
                }
                false
            })

            ivClose.setOnClickListener {
                showCloseDialog()
            }
        }
    }

    private fun showCloseDialog() {
        val fm: FragmentManager = supportFragmentManager
        val dialog = CloseDialog("Yakin untuk menutup transaksi ini ?", this)
        dialog.show(fm, CloseDialog::javaClass.name)
    }

    override fun setAdapter() {
    }

    override fun setContent() {
        viewModel.loading.observe(this, Observer {
            if (it) binding.progressBar.show()
            else binding.progressBar.hide()
        })
    }

    override fun loadData() {
        if (TRANSACTION_ID != "0")
            viewModel.selectedTrx(TRANSACTION_ID!!, moveType).observe(this, Observer {
                if (it.code == HttpCode.SUCCESS) {
                    binding.inputProductCode.requestFocus()
                    generateTable(it.data.detail)
                    toast(it.message?.get(0))
                } else {
                    toast(it.message?.get(0))
                }
            })
    }


    override fun onCloseDialog() {
        viewModel.confirmTrx(TRANSACTION_ID!!, moveType).observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                val i = Intent(this, ActiveTransactionActivity::class.java)
                i.putExtra(Constants.MOVE_TYPE, moveType)
                this.startActivity(i)
                toast(it.message?.get(0))
            } else {
                toast(it.message?.get(0))
            }
        })
    }
}