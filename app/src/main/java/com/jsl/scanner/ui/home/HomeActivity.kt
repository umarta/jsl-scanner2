package com.jsl.scanner.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityHomeBinding
import com.jsl.scanner.ui.assembling.active.ListActivity
import com.jsl.scanner.ui.assembling.active.ListPenerimaanActivity
import com.jsl.scanner.ui.auth.LoginActivity
import com.jsl.scanner.ui.dialog.CloseDialog
import com.jsl.scanner.ui.inout.InOutListActivity
import com.jsl.scanner.ui.receive.MaterialReceiveListActivity
import com.jsl.scanner.ui.scan.CheckStockActivity
import com.jsl.scanner.ui.scan.ScanActivity
import com.jsl.scanner.utils.BaseApp
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(), BaseApp.Listener, CloseDialog.Listener {

    private lateinit var binding: ActivityHomeBinding

    @Inject
    lateinit var userManager: UserManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        BaseApp(this).set()
    }

    override fun getIntentData() {
    }

    override fun setOnClick() {
        binding.apply {
            tvOpName.setOnClickListener {
                startActivity(Intent(this@HomeActivity, ScanActivity::class.java))
            }
            tvCheckStock.setOnClickListener {
                startActivity(Intent(this@HomeActivity, CheckStockActivity::class.java))
            }
            tvInventMove.setOnClickListener {
                val i = Intent(this@HomeActivity, InOutListActivity::class.java)
                this@HomeActivity.startActivity(i)

//            startActivity(Intent(this, ActiveTransactionActivity::class.java))
            }
//        tv_invent_move_in.setOnClickListener {
//            val i = Intent(this, ActiveTransactionActivity::class.java)
//            i.putExtra(Constants.MOVE_TYPE, "Penerimaan Barang")
//            this.startActivity(i)
////            startActivity(Intent(this, ActiveTransactionActivity::class.java))
//        }

            binding.tvInventMoveAssembly.setOnClickListener {
                startActivity(Intent(this@HomeActivity, ListActivity::class.java))
            }
            tvPenerimaanAssembly.setOnClickListener {
                startActivity(Intent(this@HomeActivity, ListPenerimaanActivity::class.java))
            }
            binding.tvReceive.setOnClickListener {
                startActivity(Intent(this@HomeActivity, MaterialReceiveListActivity::class.java))

            }
            tvLogout.setOnClickListener {
                val dialog = CloseDialog("Anda Yakin Ingin Keluar", this@HomeActivity)
                dialog.show(supportFragmentManager, CloseDialog::class.java.name)
            }
        }
    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
    }

    override fun onCloseDialog() {
        userManager.logout()
        this.finishAffinity()
        startActivity(Intent(this, LoginActivity::class.java))
    }

}