package com.jsl.scanner.ui.auth

import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MotionEvent
import android.view.Window
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.data.user.UserManager
import com.jsl.scanner.databinding.ActivityLoginBinding
import com.jsl.scanner.ui.home.HomeActivity
import com.jsl.scanner.ui.scan.ScanViewModel
import com.jsl.scanner.utils.BaseApp
import com.jsl.scanner.utils.hide
import com.jsl.scanner.utils.show
import com.jsl.scanner.utils.toast
import com.jsl.scanner.vo.HttpCode
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), BaseApp.Listener {

    @Inject
    lateinit var userManager: UserManager
    private val viewModel: AuthenticationViewModel by viewModels()
    private val viewModelScan: ScanViewModel by viewModels()
    private var wh: IdNameData = IdNameData("", "")
    private lateinit var server: IdNameData
    private lateinit var endpoint: String;

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        if (userManager.isLogin) {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(binding.root)
//        tv_signIn.setOnClickListener {
//            validation()
//        }

        binding.apply {
            inputPassword.setOnTouchListener(fun(v, event: MotionEvent): Boolean {
                val draw_right = 2
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= inputPassword.right - inputPassword.compoundDrawables[draw_right].bounds.width()) {
                        if (inputPassword.transformationMethod != null) {
                            inputPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_eye_off_grey,
                                0
                            )
                            inputPassword.transformationMethod = null
                        } else {
                            inputPassword.transformationMethod = PasswordTransformationMethod()
                            inputPassword.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_eye_grey,
                                0
                            )
                        }
                        return true
                    }
                }
                return false
            })

            tvSet.setOnClickListener {
                val address = inputIp.text;
                val url = android.util.Patterns.IP_ADDRESS.matcher(address.toString()).matches();
                endpoint = if (!url) {
                    "https://$address/api/"
                } else {
                    "http://$address/api/"
                }

                userManager.setServer(endpoint)
                llSet.hide()
                rlWarehouse.show()
                warehouse("")
                tilUsername.show()
                tilPassword.show()
                tvSignIn.show()

            }


            spWarehouse.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    wh = item
                }
            )
        }

        BaseApp(this).set()
    }


    private fun validation() {
        val username = binding.inputUsername.text.toString().trim()
        val password = binding.inputPassword.text.toString()

        if (wh.id.isEmpty()) {
            toast("Please Choose Warehouse")
            return
        }

        if (username.isEmpty()) {
            binding.inputUsername.error = "Username Empty"
            return
        }
        if (password.isEmpty()) {
            binding.inputPassword.error = "Password Empty"
            return
        }



        login(username, password)
    }

    private fun login(username: String, password: String) {
//        Utils.hideSoftKeyboard(this)
        binding.progressBar.show()
        val req = HashMap<String, Any>()
        req["username"] = username
        req["password"] = password
        req["warehouse_id"] = wh.id

        viewModel.login(req).observe(this, Observer {
            if (it.code == HttpCode.SUCCESS) {
                userManager.loginSaveUserData(it.data)
                userManager.setWarehouse(wh.id, wh.name)
                finishAffinity()
                binding.progressBar.hide()
                startActivity(Intent(this, HomeActivity::class.java))
            } else {
                toast(it.message?.get(0))
                binding.progressBar.hide()
            }
        })
    }

    override fun getIntentData() {
    }

    private fun warehouse(query: String) {
        viewModelScan.warehouse(query).observe(this, Observer {
            if (it.success!!) {
                val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(this, it.data)
                binding.spWarehouse.setAdapter(spinnerAdapter)
                Log.e("Warehouse", "warehouse: Finished ..")
            }
        })
    }

    override fun setOnClick() {
        binding.tvSignIn.setOnClickListener {
            validation()
        }
    }

    override fun setAdapter() {
    }

    override fun setContent() {
    }

    override fun loadData() {
    }

}