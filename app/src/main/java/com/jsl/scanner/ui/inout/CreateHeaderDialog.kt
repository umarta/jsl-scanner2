package com.jsl.scanner.ui.inout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.jsl.scanner.R
import com.jsl.scanner.data.remote.response.IdNameData
import com.jsl.scanner.databinding.DialogGenerateShiftBinding
import java.util.Date

class CreateHeaderDialog(
    val message: String,
    val listener: Listener,
    val whData: MutableList<IdNameData>
) : DialogFragment() {

    lateinit var date: Date
    private var wh_from: String = "";
    private var wh_to: String = "";

    private lateinit var binding: DialogGenerateShiftBinding

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawableResource(R.color.transparent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        binding = DialogGenerateShiftBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            tvText.text = message
            val spinnerAdapter = MaterialSpinnerAdapter<IdNameData>(activity, whData)
            spWarehouse.setAdapter(spinnerAdapter)
            spWhTo.setAdapter(spinnerAdapter)

            tvCancel.setOnClickListener {
                listener.cancelClose()
                dismiss()

            }
            tvAction.setOnClickListener {
                listener.onCloseDialog(wh_from, wh_from)

                dismiss()
            }
            spWarehouse.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    wh_from = item.id
                }
            )
            spWhTo.setOnItemSelectedListener(
                MaterialSpinner.OnItemSelectedListener<IdNameData> { view, position, id, item ->
                    wh_to = item.id
                }
            )
        }
    }

    interface Listener {
        fun onCloseDialog(wh_from: String, wh_to: String)
        fun cancelClose()
    }

}