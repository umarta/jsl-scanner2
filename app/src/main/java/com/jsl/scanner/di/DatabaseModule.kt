package com.jsl.scanner.di

//import androidx.room.Room
//import com.jsl.scanner.data.local.room.AppDatabase
//import com.jsl.scanner.data.local.room.HomeDao
//import com.jsl.scanner.data.local.room.NotifyDao
//import com.jsl.scanner.data.local.room.SearchDao
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

//    @Singleton
//    @Provides
//    fun provideAppDatabase(@ApplicationContext applicationContext: Context): AppDatabase {
//        return Room.databaseBuilder(
//            applicationContext, AppDatabase::class.java, "AutolokaDb")
//            .build()
//    }
//
//    @Singleton
//    @Provides
//    fun provideSearchDao(appDatabase: AppDatabase): SearchDao = appDatabase.searchDao()
//
//    @Singleton
//    @Provides
//    fun provideHomeDao(appDatabase: AppDatabase): HomeDao = appDatabase.homeDao()
//
//    @Singleton
//    @Provides@@
//    fun provideNotifyDao(appDatabase: AppDatabase): NotifyDao = appDatabase.notifyDao()
}