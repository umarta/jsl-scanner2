package com.jsl.scanner.di

import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object AnalyticsModule {

    @Singleton
    @Provides
    fun provideFirebaseAnalytics(@ApplicationContext applicationContext: Context): FirebaseAnalytics {
        return Firebase.analytics
    }
}